const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
var pdf = require('html-pdf');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
var http = require('http');
var moment = require('moment');
//const sgMail = require('@sendgrid/mail');
//sgMail.setApiKey('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');
exports.createProcessors = async function (req, res) {
    fields = req.body;

    let company = fields.company;
    let email = fields.email;
    let phone = fields.phone;
    let address1 = fields.address;
    let address2 = fields.address2;
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let county_Num = fields.county_Num;
    let monthly_Report = fields.monthly_Report;
    let assessType = fields.assessType;
    let elevGroup = fields.elevGroup;
    let pType = fields.pType;
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;
    let startMonth = fields.startMonth ? fields.startMonth : '';
    let endMonth = fields.endMonth ? fields.endMonth : '';
    let startyear = fields.startyear;
    let endyear = fields.endyear;
    // let send_email = fields.send_email ? fields.send_email :0;
    let STEmail = fields.STEmail ? fields.STEmail : 'False';
    if (STEmail == 'true' || STEmail == true) {
        STEmail = 'True'
    } else {
        STEmail = 'False'
    }
    if (company == '') {

        res.json({ status: "fail", message: 'Company  is required' });
        return;

    }
    // else if (email == '') {

    //     res.json({ status: "fail", message: 'email is required' });
    //     return;

    // } 
    // else if (phone == '') {

    //     res.json({ status: "fail", message: 'phone is required' });
    //     return;

    // } 
    else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({status: "fail",message: 'address2 is required'});
    //     return;

    // } 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    } else if (county_Num == '') {

        res.json({ status: "fail", message: 'county num is required' });
        return;

    } else if (monthly_Report == '') {

        res.json({ status: "fail", message: 'Monthly report is required' });
        return;

    } else if (assessType == '') {

        res.json({ status: "fail", message: 'assessType is required' });
        return;

    }


    // let data = {
    //     where: { 'email': email, 'status !=': 2 }
    // }

    // commonModel.getAll(constant.PROCESSORS, data).then(async function (user) {

    //     user = user[0];
    //     if (user && user.email != '') {

    //         res.json({ status: "fail", message: 'Email id already added' });
    //         return;

    //     } else {


    var option = {

        'company': company,
        'email': email,
        'phone': phone,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'zip': zip,
        'county_Num': county_Num,
        'monthly_Report': monthly_Report,
        'assessType': assessType,
        'elevGroup': elevGroup,
        'type': pType,
        'startMonth': startMonth,
        'endMonth': endMonth,
        'startyear': startyear,
        'endyear': endyear,
        'endMonth': endMonth,
        'created_by': created_by,
        'updated_by': updated_by,
        // 'send_email':send_email,
        'STEmail': STEmail,

    }

    commonModel.save(constant.PROCESSORS, option).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Processor Added successfully', 'data': user });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
    //     }

    // }).catch(function (error) {

    //     res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
    //     return;
    // });

}


exports.updateProcessors = async function (req, res) {

    fields = req.body;
    let company = fields.company;
    let email = fields.email;
    let phone = fields.phone;
    let address1 = fields.address;
    let address2 = fields.address2;
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let county_Num = fields.county_Num;
    let monthly_Report = fields.monthly_Report;
    let assessType = fields.assessType;
    let elevGroup = fields.elevGroup;
    let pType = fields.pType;
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;
    let startMonth = fields.startMonth;
    let endMonth = fields.endMonth;
    let startyear = fields.startyear;
    let endyear = fields.endyear;
    // let send_email = fields.send_email ? fields.send_email :0;
    let STEmail = fields.STEmail ? fields.STEmail : 'False';
    if (STEmail == 'true' || STEmail == true) {
        STEmail = 'True'
    } else {
        STEmail = 'False'
    }

    let id = req.params.id;


    if (company == '') {

        res.json({ status: "fail", message: 'Company  is required' });
        return;

    }
    // else if (email == '') {

    //     res.json({ status: "fail", message: 'email is required' });
    //     return;

    // } else if (phone == '') {

    //     res.json({ status: "fail", message: 'phone is required' });
    //     return;

    // } 
    else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({ status: "fail", message: 'address2 is required' });
    //     return;

    // } 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    } else if (county_Num == '') {

        res.json({ status: "fail", message: 'county num is required' });
        return;

    } else if (monthly_Report == '') {

        res.json({ status: "fail", message: 'Monthly report is required' });
        return;

    } else if (assessType == '') {

        res.json({ status: "fail", message: 'assessType is required' });
        return;
    }
    //  else if (send_email == '') {

    //     res.json({ status: "fail", message: 'Send email is required' });
    //     return;

    // }


    // let data = {
    //     where: {
    //         'email': email,
    //         'processor_id != ': id,
    //         'status !=': 2
    //     }
    // }

    // commonModel.getAll(constant.PROCESSORS, data).then(async function (user) {

    //     user = user[0];

    //     if (user && user.email != '') {

    //         res.json({ status: "fail", message: 'Email id already added' });
    //         return;

    //     } else {


    var option = {

        'company': company,
        'email': email,
        'phone': phone,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'zip': zip,
        'county_Num': county_Num,
        'monthly_Report': monthly_Report,
        'assessType': assessType,
        'elevGroup': elevGroup,
        'type': pType,
        'startMonth': startMonth,
        'endMonth': endMonth,
        'startyear': startyear,
        'endyear': endyear,
        'created_by': created_by,
        'updated_by': updated_by,
        'STEmail': STEmail,
        // 'send_email':send_email,
    }

    let where = { 'processor_id': id };

    commonModel.updateData(constant.PROCESSORS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Processor updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
    //     }

    // }).catch(function (error) {

    //     res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
    //     return;
    // });

}

exports.updateProcessorsStatus = async function (req, res) {

    fields = req.body;

    let status = req.params.status;
    let id = req.params.id;

    if (status == '') {

        res.json({ status: "fail", message: 'status is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    var option = {

        'status': status,
    }

    let where = { 'processor_id': id };

    commonModel.updateData(constant.PROCESSORS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Processors updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.getProcessorsById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.PROCESSORS, { where: { "processor_id": id } }).then(function (rs) {

        if (rs.length) {
            if (rs[0].STEmail == 'true' || rs[0].STEmail == 'True') {
                rs[0].STEmail = true;
            } else {
                rs[0].STEmail = false;
            }
            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.listUProcessors = function (req, res) {

    let search = req.params.search;
    data = { 
        where: { "status != ": 2 },
        orderBy: "processor_id DESC"
 };
    
    if (search != 0) {

        if (isNaN(search)) {
            data['like'] = { 'company': search };
        } else {
            data['where']['processor_id'] = search;
        }

    }
    commonModel.getAll(constant.PROCESSORS, data).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', data: [] });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}


exports.getProcessors = function (req, res) {


    commonModel.getAll(constant.PROCESSORS, { where: { "status": 1 }, orderBy: "company ASC" }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}

exports.sendEmail = async function (req, res) {
    // console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa11111111111111',req.body.new_date);
    // var a = new Date(req.body.new_date);
    // console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2222222222222222222',a);
    // var current_date = moment().toISOString();
    // console.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB',current_date);
    
    // //var cc = moment(current_date).isSame(req.body.new_date); 
    // var cc = moment.utc(a).diff(moment.utc(current_date));
    // console.log('CCCCCCCCCCCCCCCCCCCCCCCCCCCCccc',cc);
    var hostname = req.headers.host;
    var origin = req.get('origin')
    let data1 = {
        where: { "processors.processor_id": req.body.processor_id },
        join: "assessments ON processors.processor_id=assessments.processor_id",
        // join1 : "processorReport ON processors.processor_id=processorReport.processor_id",
        //cols : 'processors.company,processors.email,assessments.date_Deposited,assessments.check_Num,assessments.deposit_Num,assessments.amount,assessments.buTons,processorReport.url'
    };

    await commonModel.getAll(constant.PROCESSORSREPORT, { where: { id: req.body.processorReportId } }).then(async function (report) {
        if (report && report[0].url) {
            await commonModel.getAll(constant.REPORT, { where: { id: report[0].reportId } }).then(async function (reportData) {
                if (reportData[0].type != 'refund') {
                    await commonModel.getAll(constant.PROCESSORS, data1).then(async function (rs) {
                        var template = '';
                        let SilageeReport = [];
                        let CornReport = [];
                        if (rs.length > 0) {
                            for (let i in rs) {
                               // rs[i].date_Deposited = new Date(rs[i].date_Deposited).toString();
                            //    rs[i].date_Deposited = moment(rs[i].date_Deposited).format("YYYY-MM-DD");
                                rs[i].amount = Number(rs[i].amount);
                                rs[i].buTons = Number(rs[i].buTons);
                                if (rs[i].acssessType == 1) {
                                    CornReport.push(rs[i])
                                } else {
                                    SilageeReport.push(rs[i])
                                }
                            }
                        } else {
                            await commonModel.getAll(constant.PROCESSORS, { where: { "processor_id": req.body.processor_id } }).then(async function (processorsData) {
                                let processors_data = {
                                    'date_Deposited': 0,
                                    'check_Num': 0,
                                    'deposit_Num': 0,
                                    'amount': 0,
                                    'buTons': 0,
                                    'company': processorsData[0].company,
                                    'email': processorsData[0].email,
                                    'processor_id': req.body.processor_id
                                }

                                if (processorsData[0].assessType == 1) {
                                    CornReport.push(processors_data);
                                } else {
                                    SilageeReport.push(processors_data);

                                }
                            })

                        }

                        if (CornReport.length > 0) {
                            var template = process.cwd() + '/app/templates/email-corne.ejs';
                            var templateData = {
                                logo: origin + '/assets/images/logos/tcpb-logo.png',
                                paymentLink: origin + '/admin/payment/' + CornReport[0].processor_id + '/' + CornReport[0].company + '/' + 'corn',
                                // logo : '',
                                magazine_name: CornReport[0].company,
                                year: moment().format("YYYY"),
                                webUserName: 'weburl',
                                user_full_name: '',
                                magazine_url: 'magazine_url',
                                'verify_link': '',
                                data: CornReport,
                                processorName: CornReport[0].company,
                                processorId: CornReport[0].processor_id,
                                reportUrl: report[0].url
                            };
                            let processorData = {
                                processorName: CornReport[0].company,
                                email: CornReport[0].email,
                                type: 'Corn'
                            }
                            if (template && templateData && processorData) {
                                await sendMail(template, templateData, processorData).then(async function (result) {
                                    //res.json({'status':'success','message':'Report send successfully'});
                                    //uncomment when aws mail 
                                    if (result && result.accepted && result.accepted.length > 0) {
                                        res.json({ 'status': 'success', 'message': 'Report sent successfully' });
                                    }
                                });
                            }


                        }

                        if (SilageeReport.length > 0) {
                            var template = process.cwd() + '/app/templates/email-Silagee.ejs';
                            var templateData = {
                                logo: origin + '/assets/images/logos/tcpb-logo.png',
                                paymentLink: origin + '/admin/payment/' + SilageeReport[0].processor_id + '/' + SilageeReport[0].company + '/' + 'silage',
                                // logo : '',
                                magazine_name: SilageeReport[0].company,
                                year: moment().format("YYYY"),
                                webUserName: 'weburl',
                                user_full_name: '',
                                magazine_url: 'magazine_url',
                                'verify_link': '',
                                data: SilageeReport,
                                processorName: SilageeReport[0].company,
                                processorId: SilageeReport[0].processor_id,
                                reportUrl: report[0].url
                            };
                            let processorData = {
                                processorName: SilageeReport[0].company,
                                email: SilageeReport[0].email,
                                type: 'Silage'
                            }
                            if (template && templateData && processorData) {
                                await sendMail(template, templateData, processorData).then(async function (result) {
                                    //res.json({'status':'success','message':'Report send successfully'}); 
                                    //uncomment when aws mail 
                                    if (result && result.accepted && result.accepted.length > 0) {
                                        res.json({ 'status': 'success', 'message': 'Report sent successfully' });
                                    }

                                });
                            }


                        }

                    });
                } else {
                    await commonModel.getAll(constant.PROCESSORS, { where: { "processor_id": req.body.processor_id } }).then(async function (processorsData) {
                        let options = {
                            email: processorsData[0].email,
                            subject: processorsData[0].company + ' Month Report"',
                            body: processorsData[0].company + " Month Report",
                        }
                        await mailRefundReport(options, report[0].url).then(async function (result) {
                            if (result && result.accepted && result.accepted.length > 0) {
                                res.json({ 'status': 'success', 'message': 'Report sent successfully' });
                            }
                        });
                    });
                }
            });
        } else {
            console.log('qqqqqqqqqqqqqqqqqqqqqqqqqq');
        }
    });
}

function sendMail(template, templateData, processorData) {
    if (template && templateData) {
        return new Promise(async (resolve, reject) => {
            ejs.renderFile(template, templateData, 'utf8', function (err, file) {
                var smtpTransport = nodemailer.createTransport({
                    // host: 'smtp.gmail.com',
                    host: 'email-smtp.us-east-1.amazonaws.com',
                    port: 465,
                    ssl: true,
                    secure: true,
                    // service: "Gmail",
                    secureConnection: false,
                    auth: {
                        user: constant.SMTP_USERNAME,
                        pass: constant.SMTP_PASSWORD
                    }

                });

                var mailOptions = {
                    from: 'tcpb@texascorn.org', // webmaster@rodpub.com,smtp@ststagingserver.com
                    to: processorData.email,
                    subject: processorData.processorName + ' Month Report"',
                    body: processorData.processorName + " Month Report",
                    html: file
                }
                smtpTransport.sendMail(mailOptions, function (error, response) {
                    if (error) {
                        console.log('111111111111', error);
                        resolve(error);
                    }
                    if (response) {
                        console.log('2222222222222222', response);
                        // res.json({'status':'success','message':'Report send successfully'});
                        resolve(response);
                    }
                });
                // locat staging email 
                // var mailOptions = {
                //     from: 'smtp@ststagingserver.com', // webmaster@rodpub.com
                //     to: processorData.email,
                //     subject: processorData.processorName + ' Month Report"',
                //     body : processorData.processorName+" Month Report",
                //     html: file
                // }
                // sgMail.send(mailOptions, function (error, response) {
                //     if (error) {
                //         console.log('111111111111', error);
                //         resolve(error);
                //     }
                //     if (response) {
                //         console.log('2222222222222222', response);
                //         // res.json({'status':'success','message':'Report send successfully'});
                //         resolve(response);
                //     }
                // });
            });
        });
    }
}

function mailRefundReport(options, url) {

    return new Promise(async (resolve, reject) => {

        var smtpTransport = nodemailer.createTransport({
            // host: 'smtp.gmail.com',
            host: 'email-smtp.us-east-1.amazonaws.com',
            port: 465,
            ssl: true,
            secure: true,
            // service: "Gmail",
            secureConnection: false,
            auth: {
                user: constant.SMTP_USERNAME,
                pass: constant.SMTP_PASSWORD
            }

        });

        var mailOptions = {
            from: 'tcpb@texascorn.org',
            to: options.email,
            subject: options.subject,
            html: options.body,
            attachments: [{
                name: 'report.csv',
                path: url,
            }],
        }

        smtpTransport.sendMail(mailOptions, function (error, response) {

            if (error) {
                console.log('111111111111', error);
                resolve(error);
            }

            if (response) {
                console.log('2222222222222222', response);
                resolve(response);
            }

        });



    });


}