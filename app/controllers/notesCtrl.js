const Common           = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
var md5 = require('md5');
var country         = require('country-state-picker');

exports.createNotes = async function(req, res) {

    fields = req.body;
	let dataId        =   fields.dataId;
	let notes        =   fields.notes;


    if (dataId == '') {

        res.json({status: "fail",message: 'dataId is required'});
        return;

    }else if (notes == '') {

        res.json({status: "fail",message: 'notes is required'});
        return;

    }


	   let data = {
            where:{'dataId' : dataId,'notes':notes,'status !=' : 2}
        }



		commonModel.getAll(constant.NOTES,data).then(async function(user) {

            user = user[0];
            if (user) {
                res.json({status: "fail", message: 'Email id already registred'});
                return;

            } else {


				var option = {

					'notes' : notes,
					'dataId' : dataId,
				}

				commonModel.save(constant.NOTES,option).then(function(user) {

					res.json({'status':'success','message':'Notes Added successfully','data':user});
					return;  

				}).catch(function(error){

			        res.json({'status':'fail','message':'Something went wrong','data':error});
			        return;
			    });
			}

		});

}


exports.updateNotes = async function(req, res) {

   fields = req.body;
    let dataId        =   fields.dataId;
    let notes        =   fields.notes;

    let id        =   req.params.id; 


    if (dataId == '') {

        res.json({status: "fail",message: 'dataId is required'});
        return;

    }else if (notes == '') {

        res.json({status: "fail",message: 'notes is required'});
        return;

    }else if (id == '') {

        res.json({status: "fail",message: 'id is required'});
        return;

    }



       let data = {
           where : {

                'notes' : notes,
                'dataId' : dataId,
                'status != ' :2,
                'id != ' : id
           }
        }
        

        commonModel.getAll(constant.NOTES,data).then(async function(user) {

            user = user[0];

           if (user) {

                res.json({status: "fail", message: 'Notes already added'});
                return;

            } else {


                var option = {
                    'notes' : notes,
                    'dataId' : dataId
                }

                let where = {'id':id};

                commonModel.updateData(constant.NOTES,option,where).then(function(user) {

                    res.json({'status':'success','message':'Notes updated successfully'});
                    return;  

                }).catch(function(error){

                    res.json({'status':'fail','message':'Something went wrong'});
                    return;
                });
            }

        }).catch(function(error){

            res.json({'status':'fail','message':'Something went wrong','data':error});
            return;
        });

}

exports.updateNotesStatus = async function(req, res) {

    let status        =   req.params.status;
    let id        =   req.params.id; 

    if (status == '') {

        res.json({status: "fail",message: 'status is required'});
        return;

    }else if (id == '') {

        res.json({status: "fail",message: 'id is required'});
        return;

    }


        var option = {

            'status' : status,
        }

        let where = {'id':id};

        commonModel.updateData(constant.NOTES,option,where).then(function(user) {

            res.json({'status':'success','message':'Notes updated successfully'});
            return;  

        }).catch(function(error){

            res.json({'status':'fail','message':'Something went wrong'});
            return;
        });

}

exports.geNotesById = async function(req, res) {

    let id        =   req.params.id; 

    if (id == '') {

        res.json({status: "fail",message: 'id is required'});
        return;

    }


    commonModel.getAll(constant.NOTES,{where : {"id" : id}}).then(function(rs) {
        
        if(rs.length){

            res.json({'status':'success','message':'ok',data:rs[0]});
            return

        }else{

            res.json({'status':'fail','message':'No record found'});
            return;

        }

    }).catch(function(error){

        res.json({'status':'fail','message':'Something went wrong','data':error});
        return;
    });

}

exports.listNotes = function(req, res) {

    let id = req.params.id;

    let data = {
         where : {"dataId " : id,'status != ' : 2},
    };


    commonModel.getAll(constant.NOTES,data).then(function(rs) {
        
        if(rs.length){

            res.json({'status':'success','message':'ok',data:rs});
            return

        }else{

            res.json({'status':'fail','message':'No record found'});
            return;

        }

    }).catch(function(error){

        res.json({'status':'fail','message':'Something went wrong','data':error});
        return;
    });
}
