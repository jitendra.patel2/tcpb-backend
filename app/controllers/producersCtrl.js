const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();

exports.createProducers = async function (req, res) {

    fields = req.body;

    let firstName = fields.firstName;
    let lastName = fields.lastName;
    let email = fields.email ? fields.email : "";
    let phone = fields.phone ? fields.phone : "";
    let address1 = fields.address;
    let address2 = fields.address2 ? fields.address2 : '';
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let county_Num = fields.county_Num ? fields.county_Num : '';
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;
    let startMonth = fields.startMonth;
    let endMonth = fields.endMonth;

    if (firstName == '') {

        res.json({ status: "fail", message: 'first name  is required' });
        return;

    } else if (lastName == '') {

        res.json({ status: "fail", message: 'last name  is required' });
        return;

    }
    // else if (email == '') {

    //     res.json({status: "fail",message: 'email is required'});

    //     return;

    // } 
    // else if (phone == '') {

    //     res.json({status: "fail",message: 'phone is required'});
    //     return;

    // } 
    else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({status: "fail",message: 'address2 is required'});
    //     return;

    // } 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    }
    // else if (county_Num == '') {

    //     res.json({status: "fail",message: 'county num is required'});
    //     return;

    // }



    //    let data = {
    //         where :{'email' : email,'status !=' : 2}
    //     }

    // 	commonModel.getAll(constant.POODUCERS,data).then(async function(user) {

    //         user = user[0];

    //        if (user) {

    //             res.json({status: "fail", message: 'Email id already added'});
    //             return;

    //         } else {


    var option = {

        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'phone': phone,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'zip': zip,
        'county_Num': county_Num,
        'created_by': created_by,
        'updated_by': updated_by,
        'startMonth': startMonth,
        'endMonth': endMonth
    }


    commonModel.save(constant.POODUCERS, option).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Producer Added successfully', 'data': user });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
    // 	}

    // }).catch(function(error){

    //     res.json({'status':'fail','message':'Something went wrong','data':error});
    //     return;
    // });

}


exports.updateProducers = async function (req, res) {

    fields = req.body;

    let firstName = fields.firstName;
    let lastName = fields.lastName;
    let email = fields.email ? fields.email : "";
    let phone = fields.phone ? fields.phone : "";
    let address1 = fields.address;
    let address2 = fields.address2;
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let county_Num = fields.county_Num;
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;
    let startMonth = fields.startMonth;
    let endMonth = fields.endMonth;

    let id = req.params.id;



    if (firstName == '') {

        res.json({ status: "fail", message: 'first name  is required' });
        return;

    } else if (lastName == '') {

        res.json({ status: "fail", message: 'last name  is required' });
        return;

    }
    // else if (email == '') {

    //     res.json({status: "fail",message: 'email is required'});
    //     return;

    // } else if (phone == '') {

    //     res.json({status: "fail",message: 'phone is required'});
    //     return;

    // } 
    else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({status: "fail",message: 'address2 is required'});
    //     return;

    //} 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    }
    // else if (county_Num == '') {

    //     res.json({status: "fail",message: 'county num is required'});
    //     return;

    // }


    //    let data = {
    //        where : {

    //         'email' : email,
    //         'producer_id != ' : id,
    //         'status !=' : 2
    //        }
    //     }

    // commonModel.getAll(constant.POODUCERS,data).then(async function(user) {

    //         user = user[0];

    //         if (user) {

    //             res.json({status: "fail", message: 'Email id already added'});
    //             return;

    //         } else {


    var option = {

        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'phone': phone,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'zip': zip,
        'county_Num': county_Num,
        'created_by': created_by,
        'updated_by': updated_by,
        'startMonth': startMonth,
        'endMonth': endMonth
    }
    let where = { 'producer_id': id };

    commonModel.updateData(constant.POODUCERS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Producer updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
    //     }

    // }).catch(function(error){

    //     res.json({'status':'fail','message':'Something went wrong','data':error});
    //     return;
    // });

}

exports.updateProducersStatus = async function (req, res) {

    fields = req.body;

    let status = req.params.status;
    let id = req.params.id;

    if (status == '') {

        res.json({ status: "fail", message: 'status is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    var option = {

        'status': status,
    }

    let where = { 'producer_id': id };

    commonModel.updateData(constant.POODUCERS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Producers updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.getProducersById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.POODUCERS, { where: { "producer_id": id } }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.listProducers = function (req, res) {
    let search = req.params.search;
    data = { where: { "status != ": 2 }, orderBy: "producer_id DESC" };


    if (search != 0) {
        if (isNaN(search)) {
            // data['like1'] = {'producers.firstName':search};
            // data['like2'] = {'producers.lastName':search};

            let search_name = search.split(" ");
            if (search.length > 1) {
                if (search_name[0]) {
                    data['like'] = { 'producers.firstName': search_name[0] };
                }
                if (search_name[1]) {
                    data['like1'] = { 'producers.lastName': search_name[1] };
                }
                // data['like1'] = { 'producers.firstName': search };
            } else {
                data['like'] = { 'producers.firstName': search };
                data['or_like'] = { 'producers.lastName': search };
            }


        } else {
            data['where']['producer_id'] = search;
        }
    }


    commonModel.getAll(constant.POODUCERS, data).then(function (rs) {
       
        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'success', 'message': 'No record found', data: [] });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}

exports.getProducers = function (req, res) {


    commonModel.getAll(constant.POODUCERS, { where: { "status": 1 }, orderBy: "firstName ASC" }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}