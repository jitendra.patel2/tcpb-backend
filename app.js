var express = require('express');
var fs = require('fs');
var app = express();
var cors = require('cors')

var port = process.env.PORT || 8443;
var path = require('path');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
const fileUpload    = require('express-fileupload');
image 	= require('./lib/image');//you can include all your lib
email 	= require('./lib/email');//you can include all your lib
moment   = require('moment');
var compression = require('compression');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

/***************mysql configuratrion********************/

let configDB = require('./config/database.js');
constant    = require('./config/constants.js');
auth         = require('./config/auth.js');
mysql        = require('mysql');
request = require("request");

//configuration ===============================================================

const QueryBuilder = require('node-querybuilder');
query = new QueryBuilder(configDB, 'mysql', 'single');

NodeCache = require( "node-cache" );
 myCache = new NodeCache();

app.use(fileUpload());
app.use(compression()); //use compression 

// routes ======================================================================
require('./config/routes.js')(app); // load our routes and pass in our app

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

//catch 404 and forward to error handler
app.use(function (req, res, next) {
	res.json({'status':'fail','message':"Sorry, page not found"});
    return;
});

app.use(function (req, res, next) {
	res.json({'status':'fail','message':"Sorry, page not found"});
    return;
});

//SSL
let isUseHTTPs = true;

var http;
console.log(isUseHTTPs);
if (isUseHTTPs) { 

	let sslKey = 'ssl/private.key';
	let sslCert = 'ssl/certificate.crt';
	let sslCabundle = 'ssl/ca_bundle.crt';

	var httpServer = require('https')

    var options = {
        key: null,
        cert: null,
        ca: null
    };

    var pfx = false;

    if (!fs.existsSync(sslKey)) {
        console.log('sslKey:\t ' + sslKey + ' does not exist.');
    } else {
        pfx = sslKey.indexOf('.pfx') !== -1;
        options.key = fs.readFileSync(sslKey);
    }

    if (!fs.existsSync(sslCert)) {
        console.log('sslCert:\t ' + sslCert + ' does not exist.');
    } else {
        options.cert = fs.readFileSync(sslCert);
    }

    if (sslCabundle) {
        if (!fs.existsSync(sslCabundle)) {
            console.log('sslCabundle:\t ' + sslCabundle + ' does not exist.');
        }
        options.ca = fs.readFileSync(sslCabundle);
    }

    if (pfx === true) {
        options = {
            pfx: sslKey
        };
    }
    app = httpServer.createServer(options,app);
} else {
  app = require('http').createServer(app);
}


//launch ======================================================================
app.listen(port);
console.log(`The magic happens on port ` + port);

exports = module.exports = app;