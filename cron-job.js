
var cron = require('node-cron');
var moment = require('moment');
var pdf = require('html-pdf');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
const Common = require('./app/models/commonModel.js');//you can include all your lib
let commonModel = new Common();
cron.schedule('15 15 7 1 * *', async () => {
      //frontend

      //staging
      //var origin = 'http://tcpb.ststagingserver.com/';
      //var baseUrl = 'http://64.225.125.189:8042';

      /*** live****/
      var origin = 'http://54.197.238.120/tcpb/';
      var baseUrl = 'http://54.197.238.120:8042';
      

    // //frontend
    // var origin = 'http://localhost:4200';
    // //backend
    // var baseUrl = 'http://localhost:8042';
    let data = {
        where: { 'status': 1, 'STEmail': "True", 'email !=': '' },
        orWhere: { 'STEmail': "true" }
    }
    commonModel.getAll(constant.PROCESSORS, data).then(function (responseData) {
        let processorData = []
        if (responseData.length > 0) {
            for (let k in responseData) {
                if (responseData[k].email != "") {
                    processorData.push(responseData[k])
                }
            }
        }

        if (processorData.length > 0) {
            insertData = {
                'processorCount': processorData.length,
                'type': 'assesments',
                'startDate': '',
                'endDate': '',
            }
            commonModel.save(constant.REPORT, insertData).then(async function (result) {
                if (result && result.insert_id) {
                    var insertId = result.insert_id;
                    for (let i in processorData) {
                        commonModel.getAll(constant.ASSESMENTS, { 'where': { 'processor_id': processorData[i].processor_id } }).then(function (assesmentData) {
                            if (assesmentData.length == 0) {
                                assesmentData.push({
                                    'processor_id': processorData[i].processor_id,
                                    'company': processorData[i].company,
                                    'email': processorData[i].email,
                                    'check_Num': '-',
                                    'deposit_Num': '-',
                                    'acssessType':processorData[i].assessType,
                                    'amount': 0,
                                    'buTons': 0
                                })
                            }
                            sendReport(processorData[i], assesmentData, baseUrl, insertId, origin).then(async function (result) {
                                if (result) {
                                    where = { 'id': insertId };
                                    set = { 'status': 1 };
                                    commonModel.updateData(constant.REPORT, set, where).then(async function (result) {
                                    });
                                }
                            });

                        }).catch(function (assesmentError) {
                            console.log('--', assesmentError);
                        });
                    }
                }

            });
        }
    }).catch(function (processorError) {
        console.log('---', processorError);
    });
});

function sendReport(processorData, assesmentData, baseUrl, insertId, origin) {
    return new Promise(async (resolve, reject) => {
        var options = { format: 'Letter' };
        tabledata = '';
        var tonsBushel = "TOTAL GRAIN<br>REPORTED IN BUSHELS";
        html = "";
        let address = processorData.address1;
        if (processorData.address2) {
            address = processorData.company + "<br>" + processorData.address1 + "<br>" + processorData.address2 + "" + processorData.city + ",<br>" + processorData.state + " " + processorData.zip;
        } else {
            address = processorData.company + "<br>" + processorData.address1 + "<br>" + processorData.city + ", " + processorData.state + " " + processorData.zip;
        }
        color = (processorData.assessType == '1') ? 'green' : 'red';
        assesmentData.forEach(function (rs) {
            color = (rs.acssessType == '1') ? 'green' : 'red';
            rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
            let date_Deposited = '-';
            let check_Num = '-';
            let deposit_Num = '-';
            let amount = '-';
            let buTons = '-';
            if (rs) {
                // if (rs.amount && rs.buTons) {
                //     amount = rs.amount ? parseInt(rs.amount) : 0;
                //     buTons = rs.buTons ? parseInt(rs.buTons) : 0;
                // }
                deposit_Num = rs.deposit_Num ? rs.deposit_Num : '-';
                check_Num = rs.check_Num ? rs.check_Num : '-';
                // amount = (amount && amount > 0) ? amount.toFixed(2) : '-';
                // buTons = (buTons && buTons > 0) ? buTons.toFixed(2) : '-';
                amount = rs.amount;
                buTons = rs.buTons
                if(amount >= 0){
                 amount = Number(rs.amount);
                 amount = amount.toFixed(2);
                }

                if(buTons >= 0){
                  buTons = Number(rs.buTons);
                  buTons = buTons.toFixed(2);
                 }
            }
            if (rs.date_Deposited) {
                date_Deposited = rs.date_Deposited ? rs.date_Deposited : '-';
            }
            html += ' <tr> <td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + date_Deposited + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + check_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + deposit_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + amount + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + buTons + '</td></tr>';
        });
        if (color != 'green') {
            tonsBushel = "TOTAL CORN SILAGE<br>REPORTED IN TONS";
        }
        tabledata = ' <table width="100%" cellpadding="0" cellspacing="0"> <tr> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">DEPOSIT<br>DATE</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">CHECK<br>NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">TCPB<br>DEPOSIT NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">AMOUNT<br>RECEIVED</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">' + tonsBushel + '</th> </tr>' + html + '</table>';
        var fontFamilyTimes = "'Times New Roman'";
        var cornLogo = (color == 'green') ? 'images/corn-green.png' : 'images/corn-red.png';
        var sealLogo = (color == 'green') ? 'images/ss-green.png' : 'images/ss-red.png';
        var remittance = "Total grain processed (in bushels) since last remittance:________________________________";
        if (color != 'green') {
            remittance = "Total corn silage processed (in tons) since last remittance:_____________________________";
        }
        var figure = "Total remitted - Figure $.01 per bushel (56 lbs.): $_______________________________________";
        if (color != 'green') {
            figure = "Total remitted - Figure $.074 per ton: $__________________________________________________";
        }
        html = '<!DOCTYPE html><html> <head> <meta charset="utf-8"/> <title></title> <style>*{margin: 0; padding: 0; box-sizing: border-box;}@media print{html{zoom: 75%;}}</style> </head> <body style="margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-kerning: normal; text-rendering: optimizeLegibility;"> <div style="width: 100%; height: 100%; margin: 0 auto;"> <div style="width: 100%; padding: 0.75in 0.75in 0.65in 0.75in; color: ' + color + ';"> <table width="100%" style="vertical-align: top;"> <tr> <th style="vertical-align: top;"><img src="' + baseUrl + '/' + cornLogo + '" style="width: 100px; margin-top: -10px;"/></th> <th> <h1 style="font-family: ' + fontFamilyTimes + ', Times, serif; font-size: 15px; width: 100%; margin: 0 auto 10px auto; text-align: center;">PROCESSOR MONTHLY ASSESSMENT REPORT TEXAS CORN PRODUCERS BOARD</h1> <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; width: 100%; text-align: center; margin: 0 0 20px 0;"> 4205 N. Interstate 27 • Lubbock, Texas 79403 • 806.763.2676<br/> www.TexasCorn.org • tcpb@texascorn.org </p></th> <th style="vertical-align: top;"><img src="' + baseUrl + '/' + sealLogo + '" style="width: 100px; margin-top: -10px;"/></th> </tr><tr> <td colspan="3"> <table width="100%" style="table-layout: fixed;"> <tr> <td style="width: 4in; vertical-align: top;"> <div style="height: 1in; font-size: 13px; line-height: 25px; font-weight: bold; color: #000; padding: 40px 30px 15px 20px; font-family: ' + fontFamilyTimes + ', Times, serif;">' + address + '</div></td><td style="vertical-align: top;"> <div style="width: 300px; float: right;"> <p style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 0 0 6px 0; text-align: center; letter-spacing: 0;">This report is due by the 10th of the month</p><div style="padding: 5px; border: 3px solid ' + color + ';"> <p style="font-size: 15px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: center; margin: 0;">GO PAPERLESS !</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;">To register for electronic monthly assessment reports, provide the information below:</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;">Email: ___________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;">__________________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;">Authorized Signature</p></div></div></td></tr></table> </td></tr><tr> <td colspan="3" style="padding-bottom: 20px;"> <table width="100%"> <tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">1.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">' + remittance + '</td></tr><tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">2.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">' + figure + '</td></tr><tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> <span style="width: 15px; height: 15px; display: inline-block; border: 1px solid ' + color + '; margin-right: 8px;"></span> </td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">This is the final report until<span>___________________________________________</span>month</td></tr></table> </td></tr><tr> <td colspan="3" style="font-size: 15px; font-weight: bold; text-align: center; font-family: ' + fontFamilyTimes + ', Times, serif; padding-bottom: 6px;">Processor Remittance Record To Date</td></tr><tr> <td colspan="3" style="height: 350px; vertical-align: top; font-family: Arial, Helvetica, sans-serif;">' + tabledata + '</td></tr><tr> <td colspan="3"> <table width="100%"> <tr> <td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 10px 20px;"> This is to certify that as a first processor of corn, this company processed and herein is remitting the corn assessment as prescribed by Texas Commodity Checkoff Law. </td></tr><tr> <td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 20px 20px;"> Records are on file in our office to substantiate the amounts shown below if requested by Texas Corn Producers Board or the Texas Department of Agriculture. </td></tr><tr> <td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td></tr><tr> <td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Processor’s Signature</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Title</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Date</td></tr><tr> <td colspan="3" style="text-align: center; font-size: 14px; font-weight: bold; padding-top: 20px; font-family: ' + fontFamilyTimes + ', Times, serif;">RETURN ONE COPY TO TCPB</td></tr></table> </td></tr></table> </div></div></body></html>';
        name = Date.now() + String(processorData.processor_id);
        var url = "";
        url = baseUrl + "/" + name + ".pdf";
        await pdf.create(html, options).toFile('./public/' + name + '.pdf', async function (err, rs) {
            return rs;
        });
        //   if (reportType != '2') {
        //   if (type == "assesments") {
        var template = '';
        let SilageeReport = [];
        let CornReport = [];
        if (assesmentData.length > 0) {
            for (let j in assesmentData) {
               // assesmentData[j].date_Deposited = new Date(assesmentData[j].date_Deposited);
                assesmentData[j].amount = Number(assesmentData[j].amount);
                assesmentData[j].buTons = Number(assesmentData[j].buTons);
                if (assesmentData[j].acssessType == 'Grain') {
                    CornReport.push(assesmentData[j])
                } else {
                    assesmentData[j].acssessType = '2';
                    SilageeReport.push(assesmentData[j])
                }
            }
        }
        if (CornReport.length > 0) {
            var template = process.cwd() + '/app/templates/email-corne.ejs';
            var templateData = {
                logo: origin + '/assets/images/logos/tcpb-logo.png',
                paymentLink: origin + '/admin/payment/' + processorData.processor_id + '/' + processorData.company + '/' + 'corn',
                // logo : '',
                magazine_name: processorData.company,
                year: moment().format("YYYY"),
                webUserName: 'weburl',
                user_full_name: '',
                magazine_url: 'magazine_url',
                'verify_link': '',
                data: CornReport,
                processorName: processorData.company,
                processorId: processorData.processor_id,
                reportUrl: url
            };
            var options = {
                'subject': processorData.company + " Month Report",
                'email': processorData.email,
                'body': processorData.company + " Month Report",
                'type': 'Corn'
            };
            response = await mailReportNew(options, template, templateData).then(async function (result) {
                return result;
            });

        }
        if (SilageeReport.length > 0) {
            var template = process.cwd() + '/app/templates/email-Silagee.ejs';
            var templateData = {
                logo: origin + '/assets/images/logos/tcpb-logo.png',
                paymentLink: origin + '/admin/payment/' + processorData.processor_id + '/' + processorData.company + '/' + 'silage',
                magazine_name: processorData.company,
                year: moment().format("YYYY"),
                webUserName: 'weburl',
                user_full_name: '',
                magazine_url: 'magazine_url',
                'verify_link': '',
                data: SilageeReport,
                processorName: processorData.company,
                processorId: processorData.processor_id,
                reportUrl: url
            };
            var options = {
                'subject': processorData.company + " Month Report",
                'email': processorData.email,
                'body': processorData.company + " Month Report",
                'type': 'Corn'
            };
            if(options && template && templateData){
                response = await mailReportNew(options, template, templateData).then(async function (result) {
                    return result;
                });
            }
            
            if (response) {
                data = {
                    'response': response,
                    'url': url
                }
                insertData = {
                    'reportId': insertId,
                    'response': JSON.stringify(response),
                    'processor_id': processorData.processor_id,
                    'url': url
                }
                commonModel.save(constant.PROCESSORSREPORT, insertData).then(function (result) {
                });
                resolve(data);
            }
        }
    });
}

function mailReportNew(options, template, templateData) {
    if (template && templateData) {
        return new Promise(async (resolve, reject) => {
            ejs.renderFile(template, templateData, 'utf8', function (err, file) {
                var smtpTransport = nodemailer.createTransport({
                    host: 'email-smtp.us-east-1.amazonaws.com',
                    port: 465,
                    ssl: true,
                    secure: true,
                    secureConnection: false,
                    auth: {
                        user: constant.SMTP_USERNAME,
                        pass: constant.SMTP_PASSWORD
                    }

                });
                var mailOptions = {
                    from: 'tcpb@texascorn.org',
                    to: options.email,
                    subject: options.subject,
                    body: options.subject,
                    html: file
                }
                smtpTransport.sendMail(mailOptions, function (error, response) {
                    if (error) {
                        console.log('111111111111', error);
                        resolve(error);
                    }
                    if (response) {
                        console.log('2222222222222222', response);
                        resolve(response);
                    }
                });
            });
        });
    } else {
        console.log('000000000000000000000');
        return
    }
}
