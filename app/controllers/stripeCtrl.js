const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
//const stripe = require('stripe')('sk_test_OEBtavbx9DZEqSC9SjnquPP9');//my
const stripe = require('stripe')('sk_test_51IQJd0JFvNX4MOvKjKwsGFunuwPjzd39RpYjt0I2W5BYJXCAVhOLTWVo2SPKuXN8MZmhIdfz3uUkK8SdjnwLUjIP00w5K7Zzn1');//sy

exports.stripePaymentCard = async function (req, res) {
    fields = req.body;
    let number = fields.number;
    let exp_month = fields.exp_month;
    let exp_year = fields.exp_year;
    let cvc = fields.cvc;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let name = fields.name;
    let title = fields.title;
    let card_holder = fields.card_holder;
    let month = fields.startMonth ? fields.startMonth : '';

    if (number == '' || !number) {
        res.json({ status: "fail", message: 'Number is required' });
        return;
    } else if (exp_month == '' || !exp_month) {
        res.json({ status: "fail", message: 'Exp_month is required' });
        return;
    } else if (exp_year == '' || !exp_year) {
        res.json({ status: "fail", message: 'Exp_year is required' });
        return;
    } else if (cvc == '' || !cvc) {
        res.json({ status: "fail", message: 'cvc is required' });
        return;
    } else if (amount == '' || !amount) {
        res.json({ status: "fail", message: 'Amount is required' });
        return;
    } else if (buTons == '' || !buTons) {
        res.json({ status: "fail", message: 'buTons required' });
        return;
    } else if (name == '' || !name) {
        res.json({ status: "fail", message: 'Name is required' });
        return;
    } else if (title == '' || !title) {
        res.json({ status: "fail", message: 'Title is required' });
        return;
    } else if (card_holder == '' || !card_holder) {
        res.json({ status: "fail", message: 'Card Holder is required' });
        return;
    }
    else {
        let cardData = {
            card: {
                number: number,
                exp_month: exp_month,
                exp_year: exp_year,
                cvc: cvc,
            }
        }
        //create stripe token
        await stripe.tokens.create(cardData).then(async function (tokensData) {
            if (tokensData && tokensData.id) {
                //create stripe customer
                await stripe.customers.create({
                    description: 'Processor Customer',
                    source: tokensData.id,
                    // email: context.email
                }).then(async function (customersData) {
                    if (customersData && customersData.id) {
                        let params = {
                            amount: amount,
                            currency: "usd",
                            customer: customersData.id,
                            description: "Charge for processor"
                        }
                        //stripe payment
                        await stripe.charges.create(params).then(async function (paymentData) {
                            var option = {
                                'amount': amount,
                                'buTons': buTons,
                                'name': name,
                                'title': title,
                                'month': month,
                                'status': paymentData.status,
                            }
                            commonModel.save(constant.STRIPEPAYMENT, option).then(function (response) {
                                res.json({ 'status': 'success', 'message': 'Payment Successfully.', 'data': paymentData });
                                return;
                            }).catch(function (error) {
                                res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                                return;
                            });

                        }).catch(function (error) {
                            res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                            return;
                        });
                    }
                }).catch(function (error) {
                    res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                    return;
                });
            }
        }).catch(function (error) {
            res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
            return;
        });
    }
}

exports.stripePayment = async function (req, res) {

    fields = req.body;
    let account_holder_name = fields.account_holder_name;
    let account_holder_type = fields.account_holder_type;
    let routing_number = fields.routing_number;
    let account_number = fields.account_number;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let name = fields.name;
    let title = fields.title;
    amount = amount * 100;
    let month = fields.startMonth ? fields.startMonth : '';

    if (account_holder_name == '' || !account_holder_name) {
        res.json({ status: "fail", message: 'Account holder name is required' });
        return;
    } else if (account_holder_type == '' || !account_holder_type) {
        res.json({ status: "fail", message: 'Account holder type is required' });
        return;
    } else if (routing_number == '' || !routing_number) {
        res.json({ status: "fail", message: 'Routing number is required' });
        return;
    } else if (account_number == '' || !account_number) {
        res.json({ status: "fail", message: 'Account number is required' });
        return;
    } else if (amount == '' || !amount) {
        res.json({ status: "fail", message: 'Amount is required' });
        return;
    } else if (buTons == '' || !buTons) {
        res.json({ status: "fail", message: 'buTons required' });
        return;
    } else if (name == '' || !name) {
        res.json({ status: "fail", message: 'Name is required' });
        return;
    } else if (title == '' || !title) {
        res.json({ status: "fail", message: 'Title is required' });
        return;
    }
    else {
        await stripe.tokens.create({
            bank_account: {
                country: 'US',
                currency: 'USD',
                account_holder_name: account_holder_name,
                account_holder_type: account_holder_type,
                routing_number: routing_number,
                account_number: account_number,
                // account_holder_name: 'Jenny Rosen',
                // account_holder_type: 'individual',
                // routing_number: '110000000',
                // account_number: '000123456789',
            },
        }).then(async function (tokensData) {
            if (tokensData && tokensData.id) {
                await stripe.customers.create({
                    description: 'Processor Customer',
                    source: tokensData.id,
                    // email: context.email
                }).then(async function (customersData) {
                    if (customersData && customersData.id) {
                        await stripe.customers.verifySource(
                            customersData.id,
                            customersData.default_source,
                            {
                                amounts: [32, 45],
                            }
                        ).then(async function (verifySourceData) {
                            if (verifySourceData && verifySourceData.status == 'verified') {
                                let params = {
                                    amount:amount,
                                    currency: "USD",
                                    customer: customersData.id,
                                    description: "Charge for processor"

                                }
                                //stripe payment
                                await stripe.charges.create(params).then(async function (paymentData) {
                                    var option = {
                                        'amount': amount/100,
                                        'buTons': buTons,
                                        'name': name,
                                        'title': title,
                                        'month': month,
                                        'status': paymentData.outcome.seller_message,
                                    }
                                    commonModel.save(constant.STRIPEPAYMENT, option).then(function (response) {
                                        res.json({ 'status': 'success', 'message': 'Payment Successfully.', 'data': paymentData });
                                        return;
                                    }).catch(function (error) {
                                        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                                        return;
                                    });

                                }).catch(function (error) {
                                    res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                                    return;
                                });
                            }
                        }).catch(function (error) {
                            res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                            return;
                        });
                    }
                }).catch(function (error) {
                    res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                    return;
                });
            } else {
                res.json({ 'status': 'fail', 'message': 'Token not created' });
            }
        }).catch(function (error) {
            res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
            return;
        });
    }
}

exports.stripetest = async function (req, res) {
    await stripe.sources.create({
        type: "ach_credit_transfer",
        currency: "usd",
        owner: { email: "abc@gmail.com" },
    }).then(async function (D1) {
        if (D1.id) {
            await stripe.customers.create({
                email: 'paying.user@example.com',
                source: D1.id,
            }).then(async function (D2) {
                if (D1.id) {
                    await stripe.charges.create({
                        amount: 800,
                        currency: 'usd',
                        customer: D2.id,
                        source: D1.id,
                    }).then(async function (D3) {
                    }).catch(function (error) {
                        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                        return;
                    });
                }
            }).catch(function (error) {
                res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                return;
            });
        }
    }).catch(function (error) {
        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
   
}
