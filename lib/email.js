var nodemailer = require('nodemailer');
var ejs = require('ejs');
var moment = require('moment');
//generating a hash
exports.sendVerificationLink = function (magazineDetail, options, base_url, newData) {
        return new Promise(async (resolve, reject) => {
        var template = process.cwd() + '/app/core/templates/newregistration.ejs';
        var templateData = {
            logo: 'http://c21638.r38.cf1.rackcdn.com/footer_' + magazineDetail.MagazineId + '.png',
            // magazine_name: magazineDetail.name,
            magazine_name: magazineDetail.first_name +  magazineDetail.last_name,
            year: moment().format("YYYY"),
            webUserName: 'weburl',
            magazine_url: 'magazine_url',
            user_full_name: '',
            //magazine_url: magazineDetail.url,
            // 'verify_link': base_url + newData['pass_key'] + '/' + options.webUserName + '/'
            'verify_link': options.base_url 
        };

        // if (process.env.NODE_ENV != "production") {

        //     options.webUserName = constant.SMTP_USERNAME;
        // }
      
        ejs.renderFile(template, templateData, 'utf8', function (err, file) {
            var smtpTransport = nodemailer.createTransport({
                // host: 'smtp.gmail.com',
                // port: 465,
                // secure: true,
                // service: "Gmail",
                // secureConnection: true,
                // auth: {
                //     user: constantt.SMTP_USERNAME,
                //     pass: constantt.SMTP_PASSWORD 
                // }
                host: 'email-smtp.us-east-1.amazonaws.com',
                port: 465,
                ssl: true,
                secure: true,
                secureConnection: false,
                auth: {
                    user: constant.SMTP_USERNAME,
                    pass: constant.SMTP_PASSWORD
                }
            });
            var mailOptions = {
                from: 'tcpb@texascorn.org',
                // to: options.webUserName,
                to: options.email,
                subject: 'Welcome to ' + magazineDetail.first_name +  magazineDetail.last_name,
                html: options.new_password
            }

            smtpTransport.sendMail(mailOptions, function (error, response) {
                // if (error) {
                //     reject(error);
                // }
                // resolve("success");
                if (error) {
                    console.log('111111111111', error);
                    resolve(error);
                }
                if (response) {
                    console.log('2222222222222222', response);
                    resolve(response);
                }

            });


        });


    });

};


exports.sendCsvEmail = function (options, url) {

    return new Promise((resolve, reject) => {


        var smtpTransport = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            service: "Gmail",
            secureConnection: true,
            auth: {
                user: constantt.SMTP_USERNAME,
                pass: constantt.SMTP_PASSWORD
            }
        });
        var mailOptions = {
            from: 'webmaster@rodpub.com',
            to: options.email,
            subject: options.subject,
            html: options.body,
            attachments: [{
                name: 'report.csv',
                path: url,
            }],
        }
        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log(error);
            console.log(response);
            if (error) {
                reject(error);
            }
            resolve("success");

        });




    });

};