const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
const ObjectsToCsv = require('objects-to-csv')
var nodemailer = require('nodemailer');
var pdf = require('html-pdf');
var ejs = require('ejs');
var moment = require('moment');

exports.sendAssessmentsReport = async function (req, res) {
    email = req.body.email;
    subject = req.body.subject;
    body = req.body.body;
    data = JSON.parse(req.body.data);
    let baseUrl = req.protocol + '://' + req.headers['host'];
    var options = { format: 'Letter' };
    type = req.body.type;
    tabledata = '';
     let address = "";
    
    //start
    var tonsBushel = "TOTAL GRAIN<br>REPORTED IN BUSHELS";
    if (type == "assesments") {
        html = "";
        data.forEach(function (rs) {
             address = rs.address1;
            if (rs.address2) {
                address = rs.company + "<br>" + rs.address1 + "<br>" + rs.address2 + "" + rs.city + ",<br>" + rs.state + " " + rs.zip;
            } else {
                address = rs.company + "<br>" + rs.address1 + "<br>" + rs.city + ", " + rs.state + " " + rs.zip;
            }
            color = (rs.acssessType == '1') ? 'green' : 'red';
            rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
            let date_Deposited = '-';
            let check_Num = '-';
            let deposit_Num = '-';
            let amount = '-';
            let buTons = '-';
            if (rs) {
                deposit_Num = rs.deposit_Num ? rs.deposit_Num : '-';
                check_Num = rs.check_Num ? rs.check_Num : '-';
                amount = rs.amount;
                buTons = rs.buTons
                if (amount >= 0) {
                    amount = Number(rs.amount);
                    amount = amount.toFixed(2);
                }
                if (buTons >= 0) {
                    buTons = Number(rs.buTons);
                    buTons = buTons.toFixed(2);
                }
                if (rs.date_Deposited) {
                    date_Deposited = rs.date_Deposited ? rs.date_Deposited : '-';
                }
            }
            html += ' <tr> <td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + date_Deposited + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + check_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + deposit_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + amount + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + buTons + '</td></tr>';
        })
        if (color != 'green') {
            tonsBushel = "TOTAL CORN SILAGE<br>REPORTED IN TONS";
        }

        tabledata = ' <table width="100%" cellpadding="0" cellspacing="0"> <tr> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">DEPOSIT<br>DATE</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">CHECK<br>NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">TCPB<br>DEPOSIT NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">AMOUNT<br>RECEIVED</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">' + tonsBushel + '</th> </tr>' + html + '</table>';
    }
    if (type == "refund") {
        html = "";
        data.forEach(function (rs) {
            address = rs.address1;
            if (rs.address2) {
                address = rs.company ? rs.company:""  + "<br>" + rs.address1 ? rs.address1:"" + "<br>" + rs.address2 ? rs.address2:"" + "" + rs.city ? rs.city:"" + ",<br>"  + rs.state ? rs.state:"" + " " +rs.zip ? rs.zip:"";
            } else {
                address = rs.company ? rs.company:""  + "<br>" + rs.address1 ? rs.address1:"" + "<br>" + rs.city ? rs.city:"" + ", " + rs.state ? rs.state:"" + " " +rs.zip ? rs.zip:"";
            }
            let amount = '-';
            let buTons = '-';
            let firstName = rs.firstName ? rs.firstName : "-";
            let lastName = rs.lastName ? rs.lastName : "-";
            let company = rs.company ? rs.company : "-";
            amount = rs.amount;
            buTons = rs.buTons
            if (amount >= 0) {
                amount = Number(rs.amount);
                amount = amount.toFixed(2);
            }
            if (buTons >= 0) {
                buTons = Number(rs.buTons);
                buTons = buTons.toFixed(2);
            }
            if (rs.acssessType) {
                rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
                color = (rs.acssessType == '1') ? 'green' : 'red';
            }
            html += ' <tr> <td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.id + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.acssessType + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + firstName + ' &nbsp;' + lastName + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + company + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + buTons + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + amount + '</td></tr>';
        });

        tabledata = '  <table width="100%" cellspacing="0" cellpadding="0"> <tr> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">REFUND ID</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">ACCESSTYPE</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">PRODUCER</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">PROCESSOR</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">BUTONS</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">AMOUNT RECEIVED</th> </tr>' + html + '</table>';
    }
    var fontFamilyTimes = "'Times New Roman'";
    var cornLogo = (color == 'green') ? 'images/corn-green.png' : 'images/corn-red.png';
    var sealLogo = (color == 'green') ? 'images/ss-green.png' : 'images/ss-red.png';
    var remittance = "Total grain processed (in bushels) since last remittance:________________________________";
    if (color != 'green') {
        remittance = "Total corn silage processed (in tons) since last remittance:_____________________________";
    }
    var figure = "Total remitted - Figure $.01 per bushel (56 lbs.): $_______________________________________";
    if (color != 'green') {
        figure = "Total remitted - Figure $.074 per ton: $__________________________________________________";
    }
    html = '<!DOCTYPE html><html> <head> <meta charset="utf-8"/> <title></title> <style>*{margin: 0; padding: 0; box-sizing: border-box;}@media print{html{zoom: 75%;}}</style> </head> <body style="margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-kerning: normal; text-rendering: optimizeLegibility;"> <div style="width: 100%; height: 100%; margin: 0 auto;"> <div style="width: 100%; padding: 0.75in 0.75in 0.65in 0.75in; color: ' + color + ';"> <table width="100%" style="vertical-align: top;"> <tr> <th style="vertical-align: top;"><img src="' + baseUrl + '/' + cornLogo + '" style="width: 100px; margin-top: -10px;"/></th> <th> <h1 style="font-family: ' + fontFamilyTimes + ', Times, serif; font-size: 15px; width: 100%; margin: 0 auto 10px auto; text-align: center;">PROCESSOR MONTHLY ASSESSMENT REPORT TEXAS CORN PRODUCERS BOARD</h1> <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; width: 100%; text-align: center; margin: 0 0 20px 0;"> 4205 N. Interstate 27 • Lubbock, Texas 79403 • 806.763.2676<br/> www.TexasCorn.org • tcpb@texascorn.org </p></th> <th style="vertical-align: top;"><img src="' + baseUrl + '/' + sealLogo + '" style="width: 100px; margin-top: -10px;"/></th> </tr><tr> <td colspan="3"> <table width="100%" style="table-layout: fixed;"> <tr> <td style="width: 4in; vertical-align: top;"> <div style="height: 1in; font-size: 13px; line-height: 25px; font-weight: bold; color: #000; padding: 40px 30px 15px 20px; font-family: ' + fontFamilyTimes + ', Times, serif;">' + address + '</div></td><td style="vertical-align: top;"> <div style="width: 300px; float: right;"> <p style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 0 0 6px 0; text-align: center; letter-spacing: 0;">This report is due by the 10th of the month</p><div style="padding: 5px; border: 3px solid ' + color + ';"> <p style="font-size: 15px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: center; margin: 0;">GO PAPERLESS !</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;">To register for electronic monthly assessment reports, provide the information below:</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;">Email: ___________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;">__________________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;">Authorized Signature</p></div></div></td></tr></table> </td></tr><tr> <td colspan="3" style="padding-bottom: 20px;"> <table width="100%"> <tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">1.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">' + remittance + '</td></tr><tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">2.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">' + figure + '</td></tr><tr> <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> <span style="width: 15px; height: 15px; display: inline-block; border: 1px solid ' + color + '; margin-right: 8px;"></span> </td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;">This is the final report until<span>___________________________________________</span>month</td></tr></table> </td></tr><tr> <td colspan="3" style="font-size: 15px; font-weight: bold; text-align: center; font-family: ' + fontFamilyTimes + ', Times, serif; padding-bottom: 6px;">Processor Remittance Record To Date</td></tr><tr> <td colspan="3" style="height: 350px; vertical-align: top; font-family: Arial, Helvetica, sans-serif;">' + tabledata + '</td></tr><tr> <td colspan="3"> <table width="100%"> <tr> <td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 10px 20px;"> This is to certify that as a first processor of corn, this company processed and herein is remitting the corn assessment as prescribed by Texas Commodity Checkoff Law. </td></tr><tr> <td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 20px 20px;"> Records are on file in our office to substantiate the amounts shown below if requested by Texas Corn Producers Board or the Texas Department of Agriculture. </td></tr><tr> <td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">_________________________</td></tr><tr> <td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Processor’s Signature</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Title</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">Date</td></tr><tr> <td colspan="3" style="text-align: center; font-size: 14px; font-weight: bold; padding-top: 20px; font-family: ' + fontFamilyTimes + ', Times, serif;">RETURN ONE COPY TO TCPB</td></tr></table> </td></tr></table> </div></div></body></html>';
    //end

    // if (type == "assesments") {
    //     html = "";
    //     data.forEach(function (rs) {
    //         color = (rs.acssessType == '1') ? 'green' : 'red';
    //         html += ' <tr><td style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.assessment_id + '</td>         <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.check_Num + '</td>         <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + moment(rs.check_Date).format("DD-MM-YYYY") + '</td>         <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.buTons + '</td> <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.amount + '</td>       </tr>';
    //     });
    //     tabledata = ' <table style="width: 100%;  margin: 0 auto;">  <tr style="line-height: 40px;"> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">ASSESSMENT ID</p></th>          <th><p style="font-size: 10x; line-height: 20px; font-weight: 100;">CHECK NUMBER</p></th>          <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">CHECK  DATE</p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">BUTONS</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">AMOUNT RECEIVED</p></th></tr>' + html + '</table>';
    // }
    // if (type == "refund") {
    //     html = "";
    //     data.forEach(function (rs) {
    //         rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
    //         color = (rs.acssessType == '1') ? 'green' : 'red';
    //         html += ' <tr><td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.id + '</td>         <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.acssessType + '</td>         <td  style="text-align: center; font-size: 8px;">' + rs.firstName + ' &nbsp;' + rs.lastName + '</td>   <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.company + '</td>        <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.buTons + '</td> <td  style="text-align: center; font-size: 8px;color:' + color + ';">' + rs.amount + '</td>       </tr>';
    //     });
    //     tabledata = '  <table style="width: 100%;  margin: 0 auto;"> <tr style="line-height: 40px;"><th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">REFUND ID</p></th>          <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">ACCESSTYPE</p></th>          <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">PRODUCER </p></th><th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">PROCESSOR </p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">BUTONS</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">AMOUNT RECEIVED</p></th></tr>' + html + '</table>';
    // }
    // html = '<!DOCTYPE html><html><head> <title></title></head><body>  <div style="width: 100%;">    <div style="width: 100%; max-width: 80%; margin: 0 auto;padding: 20px;">    <table style="width: 100%; max-width: 80%; margin: 0 auto;">       <tr>        <th><img src="' + baseUrl + '/logo@x1.png"/ style="width: 100px;"></th>      <th><p style="font-size:10px;">PROCESSOR MONTHLY ASSESSMENT REPORTTEXAS CORN PRODUCERS BOARD</p><p style="font-size: 8px;">4205 N. Interstate 27 • Lubbock, Texas 79403 • 806.763.2676www.TexasCorn.org • tcpb@texascorn.org</p></th>        <th><img src="' + baseUrl + '/Selection_015.png" style="width: 100px;" /></th>     </tr>      <tr>         <table style="width: 100%; max-width: 80%; margin: 0 auto;">           <tr>            <th style="width: 20%;"></th>            <th style="width: 20%;"></th>            <th style="width: 40%;">              <p style="font-size: 8px;margin: 0px;text-align: left;">This report is due by the 10th of the month</p>               <div style="width: 100%; max-width:350px; padding: 15px; border: 1px solid #000000; ">                <p style="font-size: 8px;margin: 0px;">GO PAPERLESS !</p>                <p style="font-size: 6px;">To register for electronic monthly assessment reports, provide the information below:</p>                <p style="text-align: left; font-size: 6px; margin: 0px;">E-mail:</p>                <p style="margin-top: 0;"> ________________________________________</p>                <p> ________________________________________</p>                <p style="font-size: 6px; margin: 0;">Authorized Signature</p>              </div>             </th>           </tr>         </table>      </tr>      <tr>         <table style="width: 100%; max-width: 500px; margin-left: 10%; margin-top: 15px; ">          <tr style="line-height: 45px;font-size: 10px;">            <td>1.</td>            <td>Total grain processed (in bushels) since last remittance:<span>_______________</span></td>  </tr>       <tr style="line-height: 45px;font-size: 10px;">            <td>2.</td>            <td>Total remitted - Figure $.01 per bushel (56 lbs.): $<span>__________________</span></td>           </tr>           <tr style="line-height: 45px;font-size: 10px;">            <td>[ ]</td>            <td>This is the final report until<span>___________________</span>month</td>          </tr>        </table>      </tr>    </table>       <p style="text-align: center; font-weight:bold; font-size: 10px; margin-bottom: 0px;">Processor Remittance Record To Date</p>     <div style="padding:5px;"></div>' + tabledata + '     <div style="padding:25px;"></div>      <div>       <p style="font-size: 10px; text-align: center;">This is to certify that as a first processor of corn, this company processed and herein is remitting the corn assessment as prescribed by Texas Commodity Checkoff Law.</p>       <p style="font-size: 8px; text-align: center;">Records are on file in our office to substantiate the amounts shown below if requested by Texas Corn Producers Board or the Texas Department of Agriculture. </p>     </div>      <div style="padding:15px;"></div>      <table style="width: 100%; margin: 0 auto;"  >       <tr>         <td>_________________</td>         <td>_________________</td>         <td>_________________</td>     </tr>       <tr>         <td><p style="font-size: 6px; text-align: center; margin-top: 0px;">Processor’s Signature</p></td>         <td><p style="font-size: 8px; text-align: center; margin-top: 0px;">Title</p></td>         <td><p style="font-size: 8px; text-align: center; margin-top: 0px;">Date</p></td>       </tr>     </table>    </div></div></body></html>';
    name = Date.now();
    url = baseUrl + "/" + name + ".pdf";
    var options = {
        'subject': req.body.subject,
        'email': req.body.email,
        'body': req.body.body,

    };

    pdf.create(html, options).toFile('./public/' + name + '.pdf', function (err, rs) {
        if (err) return console.log(err);

        var smtpTransport = nodemailer.createTransport({
            host: 'email-smtp.us-east-1.amazonaws.com',
            port: 465,
            ssl: true,
            secure: true,
            // service: "Gmail",
            secureConnection: false,
            auth: {
                user: constant.SMTP_USERNAME,
                pass: constant.SMTP_PASSWORD
            }
        });
        // var mailOptions = {
        //     from: '',
        //     to: options.email,
        //     subject: options.subject,
        //     html: options.body,
        //     attachments: [{
        //         name: 'report.csv',
        //         path: url,
        //     }],
        // }
        // smtpTransport.sendMail(mailOptions, function (error, response) {
        //     res.json({ 'status': 'success', 'message': 'Report send successfully' });
        //     return;

        // });


        var mailOptions = {
            from: 'tcpb@texascorn.org', // webmaster@rodpub.com,smtp@ststagingserver.com
            to: options.email,
            subject: options.subject,
            html: options.body,
            attachments: [{
                name: 'report.csv',
                path: url,
            }],
        }
        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                console.log('111111111111', error);
                res.json({ 'status': 'error', 'message': 'Report not send' });
                return;
            }
            if (response) {
                console.log('2222222222222222', response);
                // res.json({'status':'success','message':'Report send successfully'});
                res.json({ 'status': 'success', 'message': 'Report send successfully' });
                return;
            }
        });

    });

}






exports.genrateCsv = async function (req, res) {

    let baseUrl = req.protocol + '://' + req.headers['host'];


    var options = { format: 'Letter' };
    var html = '<!DOCTYPE html><html><head> <title></title></head><body> <div style="width: 100%;"> <div style="width: 100%; max-width: 60%; margin: 0 auto;padding: 50px;">      <table style="width: 100%; max-width: 80%; margin: 0 auto;">       <tr>        <th><img src="' + baseUrl + '/logo@x1.png"/></th>        <th>PROCESSOR MONTHLY ASSESSMENT REPORTTEXAS CORN PRODUCERS BOARD<p>4205 N. Interstate 27 • Lubbock, Texas 79403 • 806.763.2676www.TexasCorn.org • TCPBb@texascorn.org</p></th>        <th><img src="' + baseUrl + '/Selection_015.png" style="width: 100px;" /></th>     </tr>     <tr>         <table style="width: 100%; max-width: 80%; margin: 0 auto;">          <tr>            <th style="width: 20%;"></th>            <th style="width: 20%;"></th>            <th style="width: 40%;">              <p style="font-size: 16px;margin: 0px;text-align: left;">This report is due by the 10th of the month</p>               <div style="width: 100%; max-width:400px; padding: 15px; border: 1px solid #000000; ">                <p style="font-size: 16px;margin: 0px;">GO PAPERLESS !</p>                <p style="font-size: 14px;">To register for electronic monthly assessment reports, provide the information below:</p>                <p style="text-align: left; font-size: 14px; margin: 0px;">E-mail:</p>                <p style="margin-top: 0;"> ________________________________________</p>                <p> ________________________________________</p>                <p style="font-size: 12px; margin: 0;">Authorized Signature</p>              </div>             </th>           </tr>         </table>      </tr>      <tr>         <table style="width: 100%; max-width: 600px; margin-left: 15%; margin-top: 15px; ">          <tr style="line-height: 45px;">            <td>1.</td>            <td>Total grain processed (in bushels) since last remittance:<span>___________________________</span></td>          </tr>           <tr>            <td>2.</td>            <td>Total remitted - Figure $.01 per bushel (56 lbs.): $<span>___________________________</span></td>          </tr>           <tr style="    line-height: 45px;">            <td>[ ]</td>            <td>This is the final report until<span>___________________________</span>month</td>               </tr>        </table>     </tr>     </table>       <p style="text-align: center; font-weight:bold; font-size: 18px; margin-bottom: 0px;">Processor Remittance Record To Date</p>      <div style="padding:5px;"></div>     <table style="width: 100%; max-width: 80%; margin: 0 auto;">       <tr style="line-height: 40px;">          <th><p>DEPOSIT DATE</p></th>         <th><p>CHECK NUMBER</p></th>          <th><p>TCPB DEPOSIT NUMBER</p></th>          <th><p>AMOUNT RECEIVED</p></th>          <th><p>TOTAL GRAIN REPORTED IN BUSHELS</p></th>       </tr>       <tr>         <td style="text-align: center;">01-08-2020</td>         <td style="text-align: center;">12345678972</td>         <td style="text-align: center;">12345678</td>         <td style="text-align: center;">25689</td>         <td style="text-align: center;">125986</td>       </tr>        <tr>         <td style="text-align: center;">01-08-2020</td>         <td style="text-align: center;">12345678972</td>         <td style="text-align: center;">12345678</td>         <td style="text-align: center;">25689</td>         <td style="text-align: center;">125986</td>       </tr>        <tr>         <td style="text-align: center;">01-08-2020</td>         <td style="text-align: center;">12345678972</td>         <td style="text-align: center;">12345678</td>         <td style="text-align: center;">25689</td>         <td style="text-align: center;">125986</td>       </tr>     </table>     <div style="padding:25px;"></div>      <div>       <p style="font-size: 12px; text-align: center;">This is to certify that as a first processor of corn, this company processed and herein is remitting the corn assessment as prescribed by Texas Commodity Checkoff Law.</p>       <p style="font-size: 12px; text-align: center;">Records are on file in our office to substantiate the amounts shown below if requested by Texas Corn Producers Board or the Texas Department of Agriculture. </p>     </div>      <div style="padding:15px;"></div>      <table style="width: 100%; max-width: 50%; margin: 0 auto;"  >      <tr>         <td>____________________________</td>         <td>____________________________</td>         <td>____________________________</td>       </tr>       <tr>         <td><p style="font-size: 10px; text-align: center; margin-top: 0px;">Processor’s Signature</p></td>         <td><p style="font-size: 10px; text-align: center; margin-top: 0px;">Title</p></td>         <td><p style="font-size: 10px; text-align: center; margin-top: 0px;">Date</p></td>       </tr>     </table>    </div></div></body></html>'
    /*  res.writeHead(200, {
          'Content-Type': 'text/html'
      });
       res.write(html);
      res.end();*/


    name = Date.now();

    url = baseUrl + "/" + name + ".pdf";

    pdf.create(html, options).toFile('./public/' + name + '.pdf', function (err, rs) {
        if (err) return console.log(err);
        res.json({ 'status': true, 'url': url });
        return true;

    });




    // list  = [{'id':1,'name':'test'},{'id':1,'name':'test'},{'id':1,'name':'test'},{'id':1,'name':'test'},{'id':1,'name':'test'},{'id':1,'name':'test'}];
    // subject = "test";
    // body = "test";
    // const csv = new ObjectsToCsv(list);
    // name = Date.now() ;
    // await csv.toDisk('./public/'+name+'.csv', { append: true })
    // let baseUrl         =   req.protocol + '://'+req.headers['host'];
    // url = baseUrl+"/"+name+".csv" ;
    // var options = {
    //     'subject' : subject,
    //     'email' : email,
    //     'body'  : body
    // };

    //  var smtpTransport = nodemailer.createTransport({
    //             host: '',
    //             port: 465,
    //             secure: true,
    //             service: "Gmail",
    //             secureConnection: true,
    //             auth: {
    //                 user: constant.SMTP_USERNAME,
    //                 pass: constant.SMTP_PASSWORD 
    //             }
    //         });
    //        var mailOptions = {
    //             from: '',
    //             to: options.email,
    //             subject: options.subject,
    //             html: options.body,
    //             attachments: [{   
    //                 name : 'report.csv',
    //                 path: url,
    //             }],
    //         }
    //         smtpTransport.sendMail(mailOptions, function(error, response) {
    //             console.log(error);
    //             console.log(response);

    //         });

    //     res.json({'status' : true,'url':url});
    //     return true;

    /* email.sendCsvEmail(option,url).then(async function(user) {
          res.json({'status' : true,'url':url,'user':user});
         return true;
     }).catch(function(error){
 
         res.json({'status':'fail','message':'Something went wrong','data':error});
         return;
     });*/

}

exports.createAssessments = async function (req, res) {

    fields = req.body;

    let processor_id = fields.processor_id;
    let county_id = fields.county_id;
    // let date_Deposited = moment(fields.date_Deposited).format("YYYY-MM-DD");
    // let check_Date = moment(fields.check_Date).format("YYYY-MM-DD");
    let date_Deposited = fields.date_Deposited;
    let check_Date = fields.check_Date;
    let check_Num = fields.check_Num;
    let deposit_Num = fields.deposit_Num;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let acssessType = fields.acssessType;
    let created_by = fields.created_by;
    let updated_by = fields.updated_by;


    if (processor_id == '') {

        res.json({ status: "fail", message: 'processor_id  is required' });
        return;

    } else if (date_Deposited == '') {

        res.json({ status: "fail", message: 'date_Deposited is required' });
        return;

    }
    else if (check_Date == '') {

        res.json({ status: "fail", message: 'check_Date is required' });
        return;

    } else if (check_Num == '') {

        res.json({ status: "fail", message: 'check_Num is required' });
        return;

    }
    else if (deposit_Num == '') {

        res.json({ status: "fail", message: 'Deposit Amount is required' });
        return;

    } else if (amount == '') {

        res.json({ status: "fail", message: 'amount is required' });
        return;

    } else if (buTons == '') {

        res.json({ status: "fail", message: 'buTons is required' });
        return;

    } else if (acssessType == '') {

        res.json({ status: "fail", message: 'acssessType is required' });
        return;

    }




    let data = {
        where: {
            'processor_id': processor_id,
            'check_Date': check_Date,
            'amount': amount,
            'status !=': 2
        }
    }

    commonModel.getAll(constant.ASSESMENTS, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Assessments already exist' });
            return;

        } else {


            var option = {

                'processor_id': processor_id,
                'county_id': county_id,
                'date_Entered': moment().format("YYYY-MM-DD"),
                // 'date_Deposited': date_Deposited ? moment(date_Deposited).format("YYYY-MM-DD"):"",
                // 'date_Deposited1': date_Deposited,
                'date_Deposited': date_Deposited,
                'check_Date': check_Date,
                'check_Num': check_Num,
                'deposit_Num': deposit_Num,
                'amount': amount,
                'buTons': buTons,
                'acssessType': acssessType,
                'created_by': created_by,
                'updated_by': updated_by,
            }

            commonModel.save(constant.ASSESMENTS, option).then(function (user) {

                res.json({ 'status': 'success', 'message': 'Assessment Added successfully', 'data': user });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}


exports.updateAssessments = async function (req, res) {

    fields = req.body;

    let processor_id = fields.processor_id;
    let county_id = fields.county_id;
    let date_Deposited = fields.date_Deposited;
    let check_Date = fields.check_Date;
    let check_Num = fields.check_Num;
    let deposit_Num = fields.deposit_Num;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let acssessType = fields.acssessType;
    let created_by = fields.created_by;
    let updated_by = fields.updated_by;

    let id = req.params.id;


    if (processor_id == '') {

        res.json({ status: "fail", message: 'processor_id  is required' });
        return;

    } else if (date_Deposited == '') {

        res.json({ status: "fail", message: 'date_Deposited is required' });
        return;

    } else if (check_Date == '') {

        res.json({ status: "fail", message: 'check_Date is required' });
        return;

    } else if (check_Num == '') {

        res.json({ status: "fail", message: 'check_Num is required' });
        return;

    } else if (deposit_Num == '') {

        res.json({ status: "fail", message: 'Deposit Amount is required' });
        return;

    } else if (amount == '') {

        res.json({ status: "fail", message: 'amount is required' });
        return;

    } else if (buTons == '') {

        res.json({ status: "fail", message: 'buTons is required' });
        return;

    } else if (acssessType == '') {

        res.json({ status: "fail", message: 'acssessType is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }



    let data = {
        where: {
            'processor_id': processor_id,
            'check_Date': check_Date,
            'amount': amount,
            'assessment_id != ': id,
            'status !=': 2
        }
    }



    commonModel.getAll(constant.ASSESMENTS, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Assessments already exist' });
            return;

        } else {


            var option = {
                'processor_id': processor_id,
                'county_id': county_id,
                'date_Entered': moment().format("YYYY-MM-DD"),
                'date_Deposited': date_Deposited,
                //'date_Deposited': date_Deposited ? moment(date_Deposited).format("YYYY-MM-DD"):"",
                // 'date_Deposited1': date_Deposited ,
                'check_Date': check_Date,
                'check_Num': check_Num,
                'deposit_Num': deposit_Num,
                'amount': amount,
                'buTons': buTons,
                'acssessType': acssessType,
                'created_by': created_by,
                'updated_by': updated_by,
            }

            let where = { 'assessment_id': id };

            commonModel.updateData(constant.ASSESMENTS, option, where).then(function (user) {

                res.json({ 'status': 'success', 'message': 'Assessment updated successfully' });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong' });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.updateAssessmentsStatus = async function (req, res) {

    fields = req.body;

    let status = req.params.status;
    let id = req.params.id;

    if (status == '') {

        res.json({ status: "fail", message: 'status is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    var option = {

        'status': status,
    }

    let where = { 'assessment_id': id };

    commonModel.updateData(constant.ASSESMENTS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Assessments updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.getAssessmentsById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.ASSESMENTS, { where: { "assessment_id": id } }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.listAssessments = function (req, res) {


    let search = req.params.search;

    // let search = req.params.search;
    // if(search!=0){

    //     data['like'] = {'processors.company':search};
    //   data['or_like'] = {'processors.company':search,'assessments.assessment_id':search,'assessments.processor_id':search};

    // }


    let data = {
        where: { "assessments.status != ": 2 },
        join: "processors ON processors.processor_id=assessments.processor_id",
        cols: "assessments.assessment_id,assessments.acssessType,assessments.buTons,assessments.check_Date,assessments.check_Num,assessments.amount,assessments.status,assessments.deposit_Num,processors.company,assessments.date_Deposited,processors.address1,processors.address2,processors.city,processors.state,processors.zip",
        orderBy: "assessments.assessment_id DESC"

    };

    let processor_id = req.params.processor_id;
    let accessType = req.params.accessType;
    let date = req.params.date;
    let date1 = req.params.date1;
    let status = req.params.status;
    let deposit_Num = req.params.deposit_Num;


    if (status == 2) {

        data['where']['assessments.status'] = 0;
    } else {

        data['where']['assessments.status'] = 1;
    }

    if (processor_id != 0) {
        data['where']['assessments.processor_id'] = processor_id;
        // data['whereIn1'] = {};
        // data['whereIn1']['key'] = 'assessments.processor_id';
        // data['whereIn1']['value'] = JSON.parse(processor_id);
    }


    /*
        if(processor_id!=0){
    
            data['where']['assessments.processor_id'] = processor_id;
        }*/


    if (accessType != 0) {

        data['where']['assessments.acssessType'] = accessType;
    }

    if (date != 0) {

        data['where']['assessments.date_Entered >='] = moment(date).format("YYYY-MM-DD");
    }

    if (date1 != 0) {

        data['where']['assessments.date_Entered <='] = moment(date1).format("YYYY-MM-DD");
    }

    if (search != 0) {

        if (isNaN(search)) {
            data['like1'] = { 'processors.company': search };
        } else {
            data['where']['assessments.processor_id'] = search;
            data['orWhere'] = { 'assessments.assessment_id': search };
        }

    }
    if (deposit_Num != 0) {
        data['where']['assessments.deposit_Num'] = deposit_Num;

    }



    commonModel.getAll(constant.ASSESMENTS, data).then(function (rs) {

        if (rs.length) {
            bTotal = 0;
            aTotal = 0;
            rs.forEach(function (rs1) {

                bTotal = Number(rs1.buTons) + bTotal;
                aTotal = Number(rs1.amount) + aTotal;

            });
            res.json({ 'status': 'success', 'message': 'ok', data: rs, bTotal: (bTotal).toFixed(2), aTotal: (aTotal).toFixed(2) });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', data: [], bTotal: 0, aTotal: 0 });
            return;

        }

    });
}



exports.listAssessments1 = function (req, res) {


    let search = req.params.search;

    // let search = req.params.search;
    // if(search!=0){

    //     data['like'] = {'processors.company':search};
    //   data['or_like'] = {'processors.company':search,'assessments.assessment_id':search,'assessments.processor_id':search};

    // }


    let data = {
        where: { "assessments.status != ": 2 },
        join: "processors ON processors.processor_id=assessments.processor_id",
        cols: "assessments.assessment_id,assessments.processor_id,assessments.acssessType,assessments.buTons,assessments.date_Entered,assessments.check_Num,assessments.amount,assessments.status,processors.company,processors.county_Num",
        orderBy: "assessments.assessment_id DESC"

    };

    let processor_id = req.params.processor_id;
    let accessType = req.params.accessType;
    let date = req.params.date;
    let date1 = req.params.date1;
    let status = req.params.status;
    let deposit_Num = req.params.deposit_Num;
    if (status == 2) {

        data['where']['assessments.status'] = 0;
    } else {

        data['where']['assessments.status'] = 1;
    }

    if (processor_id != 0) {
        data['where']['assessments.processor_id'] = processor_id;
        // data['whereIn1'] = {};
        // data['whereIn1']['key'] = 'assessments.processor_id';
        // data['whereIn1']['value'] = JSON.parse(processor_id);
    }


    /*
        if(processor_id!=0){
    
            data['where']['assessments.processor_id'] = processor_id;
        }*/


    if (accessType != 0) {

        data['where']['assessments.acssessType'] = accessType;
    }

    if (date != 0) {

        data['where']['assessments.date_Entered >='] = moment(date).format("YYYY-MM-DD");
    }

    if (date1 != 0) {

        data['where']['assessments.date_Entered <='] = moment(date1).format("YYYY-MM-DD");
    }

    if (search != 0) {

        if (isNaN(search)) {
            data['like1'] = { 'processors.company': search };
        } else {
            data['where']['assessments.processor_id'] = search;
            data['orWhere'] = { 'assessments.assessment_id': search };
        }

    }
    if (deposit_Num != 0) {
        data['where']['assessments.deposit_Num'] = deposit_Num;
    }

    let baseUrl = req.protocol + '://' + req.headers['host'];

    commonModel.getAll(constant.ASSESMENTS, data).then(async function (rs) {
        let assiment_obj = {};
        let county_ids = [];
        for (let i in rs) {
            rs[i].acssessType = (rs[i].acssessType == '1') ? 'Grain' : 'Silage';
            rs[i].date_Entered = moment(rs[i].date_Entered).format("MM-DD-YYYY")
            rs[i].buTons = Number(rs[i].buTons);
            rs[i].amount = Number(rs[i].amount);
            if (!assiment_obj[rs[i].county_Num]) {
                assiment_obj[rs[i].county_Num] = [];
                county_ids.push(rs[i].county_Num);
            }
            assiment_obj[rs[i].county_Num].push(rs[i]);
        }


        let county_data = {
            'whereIn': { 'key': 'County_Num', 'value': county_ids }
        };
        let countyResponse = await commonModel.getAll(constant.COUNTIES, county_data)

        let county_obj = {};
        let region_ids = [];
        for (let j in countyResponse) {
            if (!county_obj[countyResponse[j].Region_Id]) {
                county_obj[countyResponse[j].Region_Id] = [];
                region_ids.push(countyResponse[j].Region_Id);
            }
            county_obj[countyResponse[j].Region_Id].push(countyResponse[j]);

        }
        let region_data = {
            'whereIn': { 'key': 'id', 'value': region_ids }
        }
        let regionResponse = await commonModel.getAll(constant.REGION, region_data)

        if (rs.length) {
            html = '';
            bTotal = 0;
            aTotal = 0;
            rs.forEach(function (rs1) {
                //rs1.acssessType = (rs1.acssessType=='1') ? 'Grain' : 'Silage';

                //html+= ' <tr style="border: 1px solid black;"><td style="text-align: center; font-size: 8px;">'+rs1.assessment_id+'</td> <td style="text-align: center; font-size: 8px;">'+rs1.processor_id+'</td>         <td  style="text-align: center; font-size: 8px;">'+rs1.company+'</td> <td  style="text-align: center; font-size: 8px;">'+rs1.acssessType+'</td>          <td  style="text-align: center; font-size: 8px;">'+ moment(rs1.date_Entered).format("MM-DD-YYYY")+'</td>         <td  style="text-align: center; font-size: 8px;">'+rs1.buTons+'</td> <td  style="text-align: center; font-size: 8px;">'+rs1.amount+'</td>       </tr>';
                bTotal = Number(rs1.buTons) + bTotal;
                aTotal = Number(rs1.amount) + aTotal;
                //  bTotal = (bTotal).toFixed(2);
                //  aTotal = (aTotal).toFixed(2);

            });

            // html1=' <tr><td style="text-align: center; font-size: 8px;border: 1px solid black;" colspan="4"  align="right"><b>Total Butons</b></td><td style="text-align: left; font-size: 8px;border: 1px solid black;" colspan="2"  align="left">'+(bTotal).toFixed(2)+'</td></tr>';
            // html1+=' <tr><td style="text-align: center; font-size: 8px;border: 1px solid black;" colspan="4"  align="right"><b>Total Amount</b></td><td style="text-align: left; font-size: 8px;border: 1px solid black;" colspan="2" align="left">'+(aTotal).toFixed(2)+'</td></tr>';
            // tabledata = ' <div style="text-align: center;">Assessments List</div><br><table style="width: 100%;  margin: 0 auto;border: 1px solid black;">  <tr style="line-height: 40px;"> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">ASSESSMENT ID</p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">PROCESSOR ID</p></th>        <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">Company</p></th>   <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">ACCESSTYPE</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">DATE ENTERED</p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">BUTONS</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">AMOUNT</p></th></tr>'+html+html1+'</table>';

            var template = process.cwd() + '/app/templates/report.ejs';
            var templateData = {
                data: rs,
                bTotal: bTotal,
                aTotal: aTotal,
                county_obj: county_obj,
                regionResponse: regionResponse,
                assiment_obj: assiment_obj
            };
            if (template && templateData) {
                ejs.renderFile(template, templateData, 'utf8', function (err, file) {

                    name = Date.now();

                    url = baseUrl + "/" + name + ".pdf";
                    var options = { format: 'Letter' };
                    pdf.create(file, options).toFile('./public/' + name + '.pdf', async function (err, rs) {
                        // pdf.create(tabledata, options).toFile('./public/'+name+'.pdf',  async function(err, rs) {

                        res.json({ 'status': 'success', 'message': 'ok', 'url': url });
                        return

                    });
                });
            }

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', url: [] });
            return;

        }

    });
}