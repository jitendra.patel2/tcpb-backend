const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
var pdf = require('html-pdf');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
var moment = require('moment');
exports.saveQuery = async function (req, res) {

    fields = req.body;


    let topic = fields.topic;
    let type = fields.type;
    let status = fields.status;
    let check_Date = (fields.check_DateData != '0') ? moment(fields.check_DateData).format("YYYY-MM-DD") : '0';
    let check_DateData1 = (fields.check_DateData1 != '0') ? moment(fields.check_DateData1).format("YYYY-MM-DD") : '0';



    let data = {
        where: {

            'topic': topic,
        }
    }

    commonModel.getAll(constant.SAVEQUERY, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Query already exist' });
            return;

        } else {


            var option = {

                'topic': topic,
                'type': type,
                'status': status,
                'check_Date': check_Date,
                'check_DateData1': check_DateData1
            }

            commonModel.save(constant.SAVEQUERY, option).then(function (user) {

                res.json({ 'status': 'success', 'message': 'Query Saved successfully' });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong' });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.listQuery = function (req, res) {


    commonModel.getAll(constant.SAVEQUERY, {}).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.getQueryById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.SAVEQUERY, { where: { "id": id } }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.createRefund = async function (req, res) {

    fields = req.body;

    let processor_id = fields.processor_id;
    let producer_id = fields.producer_id;
    let check_Date = moment(fields.check_Date).format("YYYY-MM-DD");
    let check_Num = fields.check_Num;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let acssessType = fields.acssessType;
    let notes = fields.notes ? fields.notes:"";
    // let process_Date = fields.process_Date ? moment(fields.process_Date).format("YYYY-MM-DD"):"";
    // let process_Date = fields.process_Date ? moment(fields.process_Date).format("YYYY-MM-DD"):"";
    // let entry_Date = fields.entry_Date ? moment(fields.entry_Date).format("YYYY-MM-DD"):"";
    let process_Date = fields.process_Date ;
    let entry_Date = fields.entry_Date ;
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;

    if (processor_id == '') {

        res.json({ status: "fail", message: 'Processor id  is required' });
        return;

    } else if (producer_id == '') {

        res.json({ status: "fail", message: 'Producer id is required' });
        return;

    } /*else if (check_Date == '') {

        res.json({status: "fail",message: 'check_Date is required'});
        return;

    } else if (check_Num == '') {

        res.json({status: "fail",message: 'check_Num is required'});
        return;

    }*/ else if (amount == '') {

        res.json({ status: "fail", message: 'amount is required' });
        return;

    } else if (buTons == '') {

        res.json({ status: "fail", message: 'buTons is required' });
        return;

    } else if (acssessType == '') {

        res.json({ status: "fail", message: 'acssessType is required' });
        return;

    } 

    // else if (notes == '') {

    //     res.json({ status: "fail", message: 'notes is required' });
    //     return;

    // }
    //  else if (process_Date == '') {

    //     res.json({ status: "fail", message: 'process_Date is required' });
    //     return;

    // }




    let data = {
        where: {

            'processor_id': processor_id,
            'producer_id': producer_id,
            'check_Date': check_Date,
            'amount': amount,
            'status !=': 2
        }
    }

    commonModel.getAll(constant.REFUND, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Refund already exist' });
            return;

        } else {


            var option = {

                'processor_id': processor_id,
                'producer_id': producer_id,
                'check_Date': check_Date,
                'check_Num': check_Num,
                'amount': amount,
                'buTons': buTons,
                'acssessType': acssessType,
                'notes': notes,
                'entry_Date':entry_Date,
                // 'entry_Date':entry_Date ? moment(entry_Date).format('YYYY-MM-DD'):"",
                // 'entry_Date1':entry_Date,
                 'process_Date': process_Date,
                'created_by': created_by,
                'updated_by': updated_by,

            }

            commonModel.save(constant.REFUND, option).then(function (user) {

                res.json({ 'status': 'success', 'message': 'Refund Added successfully', 'data': user });
                return;

            }).catch(function (error) {
                res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}


exports.updateRefund = async function (req, res) {

    fields = req.body;

    let processor_id = fields.processor_id;
    let producer_id = fields.producer_id;
    let check_Date = moment(fields.check_Date).format("YYYY-MM-DD");
    let check_Num = fields.check_Num;
    let amount = fields.amount;
    let buTons = fields.buTons;
    let acssessType = fields.acssessType;
    let notes = fields.notes ? fields.notes:"";
    let process_Date = fields.process_Date;
    let entry_Date = fields.entry_Date;
    let created_by = fields.created_by ? fields.created_by : 1;
    let updated_by = fields.updated_by ? fields.updated_by : 1;

    let id = req.params.id;


    if (processor_id == '') {

        res.json({ status: "fail", message: 'processor_id  is required' });
        return;

    } else if (producer_id == '') {

        res.json({ status: "fail", message: 'producer_id is required' });
        return;

    } else if (check_Date == '') {

        res.json({ status: "fail", message: 'check_Date is required' });
        return;

    }
    // else if (check_Num == '') {

    //     res.json({status: "fail",message: 'check_Num is required'});
    //     return;

    // } 
    else if (amount == '') {

        res.json({ status: "fail", message: 'amount is required' });
        return;

    } else if (buTons == '') {

        res.json({ status: "fail", message: 'buTons is required' });
        return;

    } else if (acssessType == '') {

        res.json({ status: "fail", message: 'acssessType is required' });
        return;

    }
    //  else if (notes == '') {

    //     res.json({ status: "fail", message: 'notes is required' });
    //     return;

    // } else if (process_Date == '') {

    //     res.json({ status: "fail", message: 'process_Date is required' });
    //     return;

    // } 
    else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }



    let data = {
        where: {
            'processor_id': processor_id,
            'producer_id': producer_id,
            'check_Date': check_Date,
            'amount': amount,
            'id != ': id,
            'status !=': 2
        }
    }

    commonModel.getAll(constant.REFUND, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Refund already exist' });
            return;

        } else {


            var option = {

                'processor_id': processor_id,
                'producer_id': producer_id,
                'check_Date': check_Date,
                'check_Num': check_Num,
                'amount': amount,
                'buTons': buTons,
                'entry_Date':entry_Date,
                // 'entry_Date':entry_Date ? moment(entry_Date).format('YYYY-MM-DD'):"",
                // 'entry_Date1':entry_Date,
                'acssessType': acssessType,
                'notes': notes,
                'process_Date': process_Date,
                'created_by': created_by,
                'updated_by': updated_by,
            }

            let where = { 'id': id };

            commonModel.updateData(constant.REFUND, option, where).then(function (user) {

                res.json({ 'status': 'success', 'message': 'Refund updated successfully' });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong' });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.updateRefundStatus = async function (req, res) {

    fields = req.body;

    let status = req.params.status;
    let id = req.params.id;

    if (status == '') {

        res.json({ status: "fail", message: 'status is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    var option = {

        'status': status,
    }

    let where = { 'id': id };

    commonModel.updateData(constant.REFUND, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'Refund updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.getRefundById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.REFUND, { where: { "id": id } }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.listRefund = async function (req, res) {

    let search = req.params.search;



    let data = {
        where: { "refund.status != ": 2 },
        join: "processors ON processors.processor_id=refund.processor_id",
        join1: "producers ON producers.producer_id=refund.producer_id",
        cols: "refund.id,refund.producer_id,refund.processor_id,refund.acssessType,refund.buTons,refund.amount,refund.status,processors.company,producers.firstName,producers.lastName,processors.address1,processors.address2,processors.city,processors.state,processors.zip",
        orderBy: "refund.id DESC"
    };

    let data2 = {
        where: { "refund.status != ": 2 },
        join: "processors ON processors.processor_id=refund.processor_id",
        join1: "producers ON producers.producer_id=refund.producer_id",
        cols: "refund.id,refund.producer_id,refund.processor_id,refund.acssessType,refund.buTons,refund.amount,refund.status,processors.company,producers.firstName,producers.lastName,processors.address1,processors.address2,processors.city,processors.state,processors.zip",
        orderBy: "refund.id DESC"
    };

    let processor_id = req.params.processor_id;
    let producer_id = req.params.producer_id;
    let date = req.params.date;
    let date1 = req.params.date1;
    let status = req.params.status;

    if (status == 2) {

        data['where']['refund.status'] = 0;
        data2['where']['refund.status'] = 0;

    } else {

        data['where']['refund.status'] = 1;
        data2['where']['refund.status'] = 1;
    }


    if (processor_id != 0) {
        data['where']['refund.processor_id'] = processor_id;
        data2['where']['refund.processor_id'] = processor_id;
        // data['whereIn1'] = {};
        // data['whereIn1']['key'] = 'refund.processor_id';
        // data['whereIn1']['value'] = JSON.parse(processor_id;);
    }


    if (producer_id != 0) {
        data['where']['refund.producer_id'] = producer_id;
        data2['where']['refund.producer_id'] = producer_id;
        // data['whereIn'] = {};
        // data['whereIn']['key'] = 'refund.producer_id';
        // data['whereIn']['value'] = JSON.parse(producer_id);
    }

    //  if(date!=0){

    //      data['where']['refund.entry_Date <='] =  moment(date).format("YYYY-MM-DD");
    //      data2['where']['refund.entry_Date <='] =  moment(date).format("YYYY-MM-DD");

    // }

    //    if(date1!=0){

    //      data['where']['refund.entry_Date >='] =  moment(date1).format("YYYY-MM-DD");
    //      data2['where']['refund.entry_Date >='] =  moment(date1).format("YYYY-MM-DD");

    // }

    if (date != 0) {
        // data['where']['refund.entry_Date >='] = moment(date).format("YYYY-MM-DD");
        // data2['where']['refund.entry_Date >='] = moment(date).format("YYYY-MM-DD");
        data['where']['refund.entry_Date >='] =date;
        data2['where']['refund.entry_Date >='] = date;
    }

    if (date1 != 0) {
        // date1 = moment(date1).format("YYYY-MM-DD");
        // date1 = moment(date1).add(1, 'd').format("YYYY-MM-DD");
         data['where']['refund.entry_Date <='] = date1;
        data2['where']['refund.entry_Date <='] = date1;
    }

    // if (date != 0) {

    //     data['where']['refund.created_at >='] = moment(date).format("YYYY-MM-DD");
    // }

    // if (date1 != 0) {
    //     date1 = moment(date1).format("YYYY-MM-DD");
    //     date1 = moment(date1).add(1,'d').format("YYYY-MM-DD");
    //      data['where']['refund.created_at <='] = date1;
    // }



    if (search != 0) {

        if (isNaN(search)) {

            // data2['like1'] = { 'processors.company': search };
            // data2['or_like'] = { 'producers.firstName': search };
             let search_name = search.split(" ");
            if (search_name.length > 1) {
                if (search_name[0]) {
                    data['like'] = { 'producers.firstName': search_name[0] };
                }
                if (search_name[1]) {
                    data['or_like'] = { 'producers.lastName': search_name[1] };
                }
                data['or_like'] = { 'processors.company': search };
            } else {
                 data['like'] = { 'producers.firstName': search };
                 data['or_like'] = { 'processors.company': search };
                // data['or_like'] = { 'producers.lastName': search };
            }
            rs2 = await commonModel.getAll(constant.REFUND, data2).then(async function (rs2) {
                rs = [];
                for (const item in rs2) {

                    rs[item] = rs2[item].id;

                };
                return rs;
            });
            data['whereIn'] = { 'key': 'refund.id', 'value': rs2 };
            //    data['like'] = {'producers.lastName':search};
        } else {
            data['where']['refund.producer_id'] = search;
            data['orWhere'] = { 'refund.processor_id': search, 'refund.id': search };
        }
    }


    commonModel.getAll(constant.REFUND, data).then(function (rs) {
        
        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', data: [] });
            return;

        }

    });
}



exports.getReportList = async function (req, res) {

    let { limit,page } =  req.query;
    let offset = limit * page;

    commonModel.getAll(constant.REPORT, { orderBy: "id DESC" ,limit,offset }).then(async function (rs) {
        if (rs.length) {
            for (const item in rs) {
                var data = {
                    where: { "reportId": rs[item].id },
                };
                rs2 = await commonModel.getAll(constant.PROCESSORSREPORT, data).then(async function (rs2) {
                    return rs2;
                });
                rs[item].processorCount = rs2.length;
                rs[item].data = rs2;

                rs[item].crd = moment(rs[item].crd).format("LLL");

            };


            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', data: [] });
            return;

        }

    });
}


exports.getReportListById = async function (req, res) {

    let id = req.params.id;
    let type = req.params.type;

    var data = {
        where: {},
        join: "processors ON processors.processor_id=processorReport.processor_id",
        cols: "processorReport.id,processorReport.processor_id,processorReport.url,processorReport.crd,processorReport.response,processors.company,processors.STEmail",
        orderBy: "id DESC"
    };

    if (type == "processors") {

        data['where']['processorReport.processor_id'] = id;

    } else {

        data['where']['processorReport.reportId'] = id;
    }


    commonModel.getAll(constant.PROCESSORSREPORT, data).then(async function (rs) {


        if (rs.length) {

            for (const item in rs) {

                rs[item].crd = moment(rs[item].crd).format("LLL");

            };


            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', data: [] });
            return;

        }

    });
}


exports.getReport = async function (req, res) {



    console.log("getreport======================>");
    var origin = req.get('origin')
    let status = req.params.status;
    let reportType = req.params.reportType;
    let processor_type = req.params.processor_type;

    if (status == '2') {
        var data = {
            where: { "refund.status != ": 2 },
            join: "processors ON processors.processor_id=refund.processor_id",
            join1: "producers ON producers.producer_id=refund.producer_id",
            cols: "refund.id,refund.producer_id,refund.processor_id,refund.acssessType,refund.buTons,refund.amount,refund.status,processors.company,producers.firstName,producers.lastName"
        };


        date = new Date();
        date.setDate(date.getDate() - 6);
        date.setFullYear(date.getFullYear() - 1);
        lastYear = date.getFullYear() + '-10-01'
        data['where']['refund.entry_Date >= '] = moment(lastYear).format("YYYY-MM-DD");
        data['where']['refund.entry_Date <= '] = moment().format("YYYY-MM-DD");


        table = constant.REFUND;

    } else {
        var data = {

            where: { "assessments.status != ": 2 },
            join: "processors ON processors.processor_id=assessments.processor_id",
            cols: "assessments.assessment_id,assessments.processor_id,assessments.acssessType,assessments.buTons,assessments.check_Date,assessments.check_Num,assessments.deposit_Num,assessments.amount,assessments.status,processors.company,assessments.date_Entered,assessments.date_Deposited"
        };


        date = new Date();
        date.setDate(date.getDate() - 6);
        date.setFullYear(date.getFullYear() - 1);
        lastYear = date.getFullYear() + '-10-01'
        data['where']['assessments.date_Entered >= '] = moment(lastYear).format("YYYY-MM-DD");
        data['where']['assessments.date_Entered <= '] = moment().format("YYYY-MM-DD");

        table = constant.ASSESMENTS;


    }

    let stEmail = req.params.stEmail;
    data1 = {};

    // if(stEmail==1){

    //     data1 = {where:{'STEmail':'True','email != ' : ''}};

    // }

    if (status == '1') {
        let date = req.params.date;
        let date1 = req.params.date1;

        data1 = {
            where: { 'assessments.status != ': 2 },
            join: "assessments ON processors.processor_id=assessments.processor_id",
            //group_by : "processors.processor_id"
        }

        if (stEmail == 1) {
            data1.where = { 'processors.STEmail': 'True', 'processors.email != ': '' };
        }
        // if (stEmail == 2) {
        //     data1.where = { 'processors.STEmail': 'False', 'processors.email != ': '' };
        // }

        data1['where']['assessments.date_Entered >= '] = moment(lastYear).format("YYYY-MM-DD");
        data1['where']['assessments.date_Entered <= '] = moment().format("YYYY-MM-DD");


    } else {
        let date = req.params.date;
        let date1 = req.params.date1;

        data1 = {
            where: { 'refund.status != ': 2 },
            join: "refund ON processors.processor_id=refund.processor_id",
            //group_by : "processors.processor_id"
        }

        if (stEmail == 1) {

            data1.where = { 'processors.STEmail': 'True', 'processors.email != ': '' };

        }
        // if (stEmail == 2) {
        //     data1.where = { 'processors.STEmail': 'False', 'processors.email != ': '' };
        // }

        data1['where']['refund.entry_Date >= '] = moment(lastYear).format("YYYY-MM-DD");
        data1['where']['refund.entry_Date <= '] = moment().format("YYYY-MM-DD");

    }

    if (processor_type != 0) {

        data1['where']['processors.type'] = processor_type;
    }

    // let new_data = {};

    // if (processor_type != 0) {

    //     if (stEmail == 1 || stEmail == "1") {
    //         new_data.where = { 'processors.STEmail': 'True', 'processors.email != ': '', 'processors.status': 1 };
    //     }  else {
    //         new_data.where = { 'processors.type': processor_type, 'processors.status': 1 };
    //     }
    // } else {
    //     new_data.where = { 'processors.status': 1 };
    // }

    let new_data = {};
    if (processor_type != 0) {
        if(stEmail == 1 || stEmail == "1"){
            new_data.where = {'processors.type': processor_type, 'processors.STEmail': 'True', 'processors.status': 1 };
        } else if(stEmail == 2 || stEmail == "2"){
            new_data.where = {'processors.type': processor_type, 'processors.STEmail': 'False',  'processors.status': 1 };
        } else {
            new_data.where = { 'processors.type': processor_type, 'processors.status': 1 };
        }

    } else {
        if(stEmail == 1 || stEmail == "1"){
            new_data.where = {'processors.STEmail': 'True','processors.status': 1 };
        } else if(stEmail == 2 || stEmail == "2"){
            new_data.where = {'processors.STEmail': 'False','processors.status': 1 };
        } else {
            new_data.where = {'processors.status': 1 };
        }
    }



    commonModel.getAll(constant.PROCESSORS, new_data).then(async function (data12) {
        //commonModel.getAll(constant.PROCESSORS, data1).then(async function(data12) {

        //res.json({'status':'success','message':'Report send successfully', data12});


        let date = req.params.date;
        let date1 = req.params.date1;
        let processorCount = (req.params.processorCount) ? req.params.processorCount : '2';

        insertData = {
            'processorCount': data12.length,
            'type': status != "2" ? 'assesments':'refund',
            'startDate': (date != 0) ? moment(date).format("YYYY-MM-DD") : '',
            'endDate': (date1 != 0) ? moment(date1).format("YYYY-MM-DD") : ''
        }

        insertId = await commonModel.save(constant.REPORT, insertData).then(async function (result) {
            return result.insertId;
        });


        data12.forEach(function (rs) {
            name = rs.company;
            let address = rs.address1;
            if (rs.address2) {
                address = rs.company + "<br>" + rs.address1 + "<br>" + rs.address2 + "" + rs.city + ",<br>" + rs.state + " " + rs.zip;
            } else {
                address = rs.company + "<br>" + rs.address1 + "<br>" + rs.city + ", " + rs.state + " " + rs.zip;
            }



            if (status != "2") {

                data['where']['assessments.processor_id'] = rs.processor_id;
                type = 'assesments';

            } else {

                data['where']['refund.processor_id'] = rs.processor_id;
                type = 'refund';

            }


            //  var data = {

            //      where : {"assessments.status != " : 2 },
            //      join : "processors ON processors.processor_id=assessments.processor_id",
            //     cols : "assessments.assessment_id,assessments.acssessType,assessments.buTons,assessments.check_Date,assessments.check_Num,assessments.amount,assessments.status,processors.company"
            // };




            // if(date!=0){

            //     data['where']['assessments.date_Entered >='] =  moment(date).format("YYYY-MM-DD");
            // }

            //    if(date1!=0){

            //     data['where']['assessments.date_Entered <='] =  moment(date1).format("YYYY-MM-DD");
            // }
            // table = constant.ASSESMENTS;
             commonModel.getAll(table, data).then(async function (rs1) {
                if(rs1.length == 0){
                    rs1.push({
                        'processor_id':rs.processor_id,
                        'company':rs.company,
                        'email':rs.email,
                        'check_Num' : '-',
                       'deposit_Num' : '-',
                       'acssessType':rs.assessType,
                       'amount':0,
                       'buTons':0
                    })
                }
                //if(rs1.length!=0){

                email = rs.email;
                subject = rs.company + " Month Report";
                body = rs.company + " Month Report";
                let baseUrl = req.protocol + '://' + req.headers['host'];

                sendReport(email, subject, body, rs1, type, baseUrl, rs.processor_id, insertId, reportType, name, address, rs.assessType, rs.company,origin).then(async function (result) {
                    if (result) {
                        where = { 'id': insertId };

                        if (reportType != 2) {
                            set = { 'status': 1 };
                        } else {
                            set = { 'status': 2 };
                        }
                        commonModel.updateData(constant.REPORT, set, where).then(async function (result) {

                        });
                    }


                });

                //}

            });

        });
        res.json({ 'status': 'success', 'message': 'Report sent successfully' });
        return;
    });

}

function sendReport(email, subject, body, data, type, baseUrl, processor_id, insertId, reportType, name, address, assessType, company,origin) {

    return new Promise(async (resolve, reject) => {

        var options = { format: 'Letter' };
        tabledata = '';
        var tonsBushel = "TOTAL GRAIN<br>REPORTED IN BUSHELS";


        if (type == "assesments") {

            html = "";
            color = (assessType == '1') ? 'green' : 'red';

           
            data.forEach(function (rs) {


                color = (rs.acssessType == '1') ? 'green' : 'red';
                rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
            
                let date_Deposited = '-';
                let check_Num = '-';
                let deposit_Num = '-';
                let amount = '-';
                let buTons = '-';
                if (rs) {
                    //  check_Num = rs.check_Num ? parseInt(rs.check_Num) : 0;
                    //  deposit_Num = rs.deposit_Num ? parseInt(rs.deposit_Num) : 0;
                    // if(rs.amount && rs.buTons){
                    //     amount = rs.amount ? parseInt(rs.amount) : 0;
                    //     buTons = rs.buTons ? parseInt(rs.buTons) : 0;
                    // } 
                    
                    // deposit_Num = (deposit_Num && deposit_Num > 0) ? deposit_Num : '-';
                    // check_Num = (check_Num && check_Num > 0) ? check_Num : '-';

                    deposit_Num = rs.deposit_Num ? rs.deposit_Num:'-';
                    check_Num = rs.check_Num ? rs.check_Num:'-';
                      amount = rs.amount;
                      buTons = rs.buTons
                      if(amount >= 0){
                       amount = Number(rs.amount);
                       amount = amount.toFixed(2);
                      }

                      if(buTons >= 0){
                        buTons = Number(rs.buTons);
                        buTons = buTons.toFixed(2);
                       }

                    //   amount = (amount && amount > 0) ? amount.toFixed(2) : '-';
                    //   buTons = (buTons && buTons > 0) ? buTons.toFixed(2) : '-';
                }


                if (rs.date_Deposited) {  
                    date_Deposited = rs.date_Deposited ? rs.date_Deposited : '-';
                    
                }
               
                //date_Entered

                html += ' <tr> <td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + date_Deposited + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + check_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + deposit_Num + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + amount + '</td><td style="text-align: center; font-size: 12px; padding: 6px; vertical-align: top; color: black !important;">' + buTons + '</td></tr>';



            });


            if (color != 'green') {
                tonsBushel = "TOTAL CORN SILAGE<br>REPORTED IN TONS";
            }

            tabledata = ' <table width="100%" cellpadding="0" cellspacing="0"> <tr> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">DEPOSIT<br>DATE</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">CHECK<br>NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">TCPB<br>DEPOSIT NUMBER</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">AMOUNT<br>RECEIVED</th> <th style="font-size: 12px; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + '; text-align: center; padding: 6px; vertical-align: top;">' + tonsBushel + '</th> </tr>' + html + '</table>';

        }

        if (type == "refund") {
            html = "";
            color = (assessType == '1') ? 'green' : 'red';
            data.forEach(function (rs) {
            if(!rs.id){
                rs.id="-";
                rs.acssessType="-";
                rs.firstName="-";
                rs.lastName="";
                rs.company="-";
            }
                // let amount = rs.amount ? rs.amount : 0;
                // let buTons = rs.buTons ? rs.buTons : 0;
                // amount = (amount && amount > 0) ? amount.toFixed(2) : '-';
                // buTons = (buTons && buTons > 0) ? buTons.toFixed(2) : '-';
                amount = rs.amount;
                buTons = rs.buTons
                if(amount >= 0){
                 amount = Number(rs.amount);
                 amount = amount.toFixed(2);
                }

                if(buTons >= 0){
                  buTons = Number(rs.buTons);
                  buTons = buTons.toFixed(2);
                 }
                 if(rs.acssessType){
                    rs.acssessType = (rs.acssessType == '1') ? 'Grain' : 'Silage';
                    color = (rs.acssessType == '1') ? 'green' : 'red';
                 }

                html += ' <tr> <td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.id + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.acssessType + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.firstName + ' &nbsp;' + rs.lastName + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + rs.company + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + buTons + '</td><td style="text-align: center; font-size: 13px; padding: 6px; vertical-align: top; color: black !important;">' + amount + '</td></tr>';

            });

            tabledata = '  <table width="100%" cellspacing="0" cellpadding="0"> <tr> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">REFUND ID</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">ACCESSTYPE</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">PRODUCER</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">PROCESSOR</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">BUTONS</th> <th style="font-size: 13px; text-align: center; padding: 6px; vertical-align: top; border-top: 2px solid ' + color + '; border-bottom: 2px solid ' + color + ';">AMOUNT RECEIVED</th> </tr>' + html + '</table>';


        }

        var fontFamilyTimes = "'Times New Roman'";
        var cornLogo = (color == 'green') ? 'images/corn-green.png' : 'images/corn-red.png';
        var sealLogo = (color == 'green') ? 'images/ss-green.png' : 'images/ss-red.png';
        // var cornLogo = 'images/logo@x1.png';
        // var sealLogo = 'images/logo@x1.png';

        var remittance = "Total grain processed (in bushels) since last remittance:________________________________";
        if (color != 'green') {
            remittance = "Total corn silage processed (in tons) since last remittance:_____________________________";
        }

        var figure = "Total remitted - Figure $.01 per bushel (56 lbs.): $_______________________________________";
        if (color != 'green') {
            figure = "Total remitted - Figure $.074 per ton: $__________________________________________________";
        }


        html = '<!DOCTYPE html><html><head><meta charset="utf-8" /><title></title><style>*{margin:0;padding:0;box-sizing:border-box}@media print{html{zoom:75%}}</style></head><body style="margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-kerning: normal; text-rendering: optimizeLegibility;"><div style="width: 100%; height: 100%; margin: 0 auto;"><div style="width: 100%; padding: 0.75in 0.75in 0.65in 0.75in; color: ' + color + ';"><table width="100%" style="vertical-align: top;"><tr><th style="vertical-align: top;"><img src="' + baseUrl + '/' + cornLogo + '" style="width: 100px; margin-top: -10px;" /></th><th><h1 style="font-family: ' + fontFamilyTimes + ', Times, serif; font-size: 15px; width: 100%; margin: 0 auto 10px auto; text-align: center;"> PROCESSOR MONTHLY ASSESSMENT REPORT TEXAS CORN PRODUCERS BOARD</h1><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; width: 100%; text-align: center; margin: 0 0 20px 0;"> 4205 N. Interstate 27 • Lubbock, Texas 79403 • 806.763.2676<br /> www.TexasCorn.org • tcpb@texascorn.org</p></th><th style="vertical-align: top;"><img src="' + baseUrl + '/' + sealLogo + '" style="width: 100px; margin-top: -10px;" /></th></tr><tr><td colspan="3"><table width="100%" style="table-layout: fixed;"><tr><td style="width: 4in; vertical-align: top;"><div style="height: 1in; font-size: 13px; line-height: 25px; font-weight: bold; color: #000; padding: 40px 30px 15px 20px; font-family: ' + fontFamilyTimes + ', Times, serif;"> ' + address + '</div></td><td style="vertical-align: top;"><div style="width: 300px; float: right;"><p style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 0 0 6px 0; text-align: center; letter-spacing: 0;"> This report is due by the 10th of the month</p><div style="padding: 5px; border: 3px solid ' + color + ';"><p style="font-size: 15px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: center; margin: 0;"> GO PAPERLESS !</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;"> To register for electronic monthly assessment reports, provide the information below:</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;"> Email: ___________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 8px 0;"> __________________________________________</p><p style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-align: center; margin: 0;"> Authorized Signature</p></div></div></td></tr></table></td></tr><tr><td colspan="3" style="padding-bottom: 20px;"><table width="100%"><tr><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> 1.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> ' + remittance + '</td></tr><tr><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> 2.</td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> ' + figure + '</td></tr><tr><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> <span style="width: 15px; height: 15px; display: inline-block; border: 1px solid ' + color + '; margin-right: 8px;"></span></td><td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 10px 0;"> This is the final report until<span>___________________________________________</span>month</td></tr></table></td></tr><tr><td colspan="3" style="font-size: 15px; font-weight: bold; text-align: center; font-family: ' + fontFamilyTimes + ', Times, serif; padding-bottom: 6px;"> Processor Remittance Record To Date</td></tr><tr><td colspan="3" style="height: 300px; vertical-align: top; font-family: Arial, Helvetica, sans-serif;">' + tabledata + '</td></tr><tr><td colspan="3"><table width="100%"><tr><td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 10px 20px;"> This is to certify that as a first processor of corn, this company processed and herein is remitting the corn assessment as prescribed by Texas Commodity Checkoff Law.</td></tr><tr><td colspan="3" style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif; padding: 0 20px 20px 20px;"> Records are on file in our office to substantiate the amounts shown below if requested by Texas Corn Producers Board or the Texas Department of Agriculture.</td></tr><tr><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> _________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> _________________________</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> _________________________</td></tr><tr><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> Processor’s Signature</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> Title</td><td style="text-align: center; font-size: 13px; font-family: Arial, Helvetica, sans-serif;"> Date</td></tr><tr><td colspan="3" style="text-align: center; font-size: 14px; font-weight: bold; padding-top: 20px; font-family: ' + fontFamilyTimes + ', Times, serif;"> RETURN ONE COPY TO TCPB</td></tr></table></td></tr></table></div></div></body></html>';        
        name = Date.now() + String(processor_id);
        var url = "";
        url = baseUrl + "/" + name + ".pdf";

        // var options = {
        //     'subject': subject,
        //     'email': email,
        //     'body': body,
        //     'type':''
        // };



        await pdf.create(html, options).toFile('./public/' + name + '.pdf', async function (err, rs) {
            return rs;
        });

        if (reportType != '2') {

            if (type == "assesments") {
                var template = '';
                let SilageeReport = [];
                let CornReport = [];
                if (data.length > 0) {
                    for (let i in data) {
                        // data[i].date_Deposited = new Date(data[i].date_Deposited);
                        data[i].amount = Number(data[i].amount);
                        data[i].buTons = Number(data[i].buTons);
                        if (data[i].acssessType == 'Grain') {
                            CornReport.push(data[i])
                        } else {
                            data[i].acssessType = '2';
                            SilageeReport.push(data[i])
                        }
                    }
                }
                
                if (CornReport.length > 0) {
                    var template = process.cwd() + '/app/templates/email-corne.ejs';
                    var templateData = {
                        logo: origin +'/assets/images/logos/tcpb-logo.png',
                        paymentLink:origin +'/admin/payment/' + processor_id + '/'+ company +'/'+ 'corn',
                        // logo : '',
                        magazine_name: company,
                        year: moment().format("YYYY"),
                        webUserName: 'weburl',
                        user_full_name: '',
                        magazine_url: 'magazine_url',
                        'verify_link': '',
                        data: CornReport,
                        processorName: company,
                        processorId : processor_id,
                        reportUrl: url
                    };
                    var options = {
                        'subject': subject,
                        'email': email,
                        'body': body,
                        'type':'Corn'
                    };
                    response = await mailReportNew(options, template, templateData).then(async function (result) {
                        return result;
                    });

                }
                if (SilageeReport.length > 0) {
                    var template = process.cwd() + '/app/templates/email-Silagee.ejs';
                    var templateData = {
                        logo: origin +'/assets/images/logos/tcpb-logo.png',
                        paymentLink:origin +'/admin/payment/' + processor_id + '/' + company + '/'+ 'silage',
                        magazine_name: company,
                        year: moment().format("YYYY"),
                        webUserName: 'weburl',
                        user_full_name: '',
                        magazine_url: 'magazine_url',
                        'verify_link': '',
                        data: SilageeReport,
                        processorName: company,
                        processorId : processor_id,
                        reportUrl: url
                    };
                    var options = {
                        'subject': subject,
                        'email': email,
                        'body': body,
                        'type':'Silage'
                    };
                    if(options && template && templateData){
                        response = await mailReportNew(options, template, templateData).then(async function (result) {
                            return result;
                        });
                    }
                    
                }


            } else {
                var options = {
                    'subject': subject,
                    'email': email,
                    'body': body,
                };
                if(options && url){
                response = await mailReport(options, url).then(async function (result) {
                    return result;
                });
            }

            }
            // response = await mailReport(options,url).then(async function(result) {
            //     return result;
            // });
        } else {

            response = "genrated"
        }

        if (response) {

            data = {
                'response': response,
                'url': url
            }

            insertData = {
                'reportId': insertId,
                'response': (response != "genrated") ? JSON.stringify(response) : 'genrated',
                'processor_id': processor_id,
                'url': url
            }

            commonModel.save(constant.PROCESSORSREPORT, insertData).then(function (result) {
            });

            resolve(data);
        }


    });


}

function mailReport(options, url) {

    return new Promise(async (resolve, reject) => {

        var smtpTransport = nodemailer.createTransport({
            // host: 'smtp.gmail.com',
            host: 'email-smtp.us-east-1.amazonaws.com',
            port: 465,
            ssl: true,
            secure: true,
            // service: "Gmail",
            secureConnection: false,
            auth: {
                user: constant.SMTP_USERNAME,
                pass: constant.SMTP_PASSWORD
            }

        });

        var mailOptions = {
            from: 'tcpb@texascorn.org',
            to: options.email,
            subject: options.subject,
            html: options.body,
            attachments: [{
                name: 'report.csv',
                path: url,
            }],
        }

        smtpTransport.sendMail(mailOptions, function (error, response) {

            if (error) {
                console.log('111111111111', error);
                resolve(error);
            }

            if (response) {
                console.log('2222222222222222', response);
                resolve(response);
            }

        });



    });


}

function mailReportNew(options,template,templateData) {
    if (template && templateData) {
        return new Promise(async (resolve, reject) => {
            ejs.renderFile(template,templateData, 'utf8', function (err, file) {
                var smtpTransport = nodemailer.createTransport({
                    // host: 'smtp.gmail.com',
                    host: 'email-smtp.us-east-1.amazonaws.com',
                    port: 465,
                    ssl: true,
                    secure: true,
                    // service: "Gmail",
                    secureConnection: false,
                    auth: {
                        user: constant.SMTP_USERNAME,
                        pass: constant.SMTP_PASSWORD
                    }

                });
                var mailOptions = {
                    from: 'tcpb@texascorn.org',
                    to: options.email,
                    subject: options.subject,
                    body : options.subject,
                    // html: options.body,
                    // to: processorData.email,
                    // subject: processorData.type + ' Report',
                    html: file
                }
                smtpTransport.sendMail(mailOptions, function (error, response) {


                    if (error) {
                        console.log('111111111111', error);
                        resolve(error);
                    }

                    if (response) {
                        console.log('2222222222222222', response);
                        resolve(response);
                    }


                });
            });
        });
    } else {
        console.log('000000000000000000000');
        return
    }
}




exports.getProcessorCount = async function (req, res) {


    let date = req.params.date;
    let date1 = req.params.date1;
    let stEmail = req.params.stEmail;
    let status = req.params.status;
    let processor_type = req.params.processor_type;


    if (status == 1) {


        data = {
            where: { 'assessments.status != ': 2,  },
            join: "assessments ON processors.processor_id=assessments.processor_id",
            //group_by : "processors.processor_id"
        }

        data.where = { 'processors.email != ': '' };

        if (stEmail == '1' || stEmail == 1) {

            data.where = { 'processors.STEmail': 'True', 'processors.email != ': '' };

        } 
        if (stEmail == '2' || stEmail == 2) {
            data.where = { 'processors.STEmail': 'False', 'processors.email != ': '' };
        }



        // if(date!='0'){

        date = new Date();
        date.setDate(date.getDate() - 6);
        date.setFullYear(date.getFullYear() - 1);
        lastYear = date.getFullYear() + '-10-01'
        data['where']['assessments.date_Entered >= '] = moment(lastYear).format("YYYY-MM-DD");
        // }

        // if(date1!='0'){

        data['where']['assessments.date_Entered <= '] = moment().format("YYYY-MM-DD");
        // }


    } else {


        data = {
            where: { 'refund.status != ': 2 },
            join: "refund ON processors.processor_id=refund.processor_id",
            //group_by : "processors.processor_id"
        }
        data.where = {  'processors.email != ': '' };
        if (stEmail == "1" || stEmail == 1) {

            data.where = { 'processors.STEmail': 'True', 'processors.email != ': '' };

        } 
        if (stEmail == "2" || stEmail == 2) {
            data.where = { 'processors.STEmail': 'False', 'processors.email != ': '' };
        }

        // if(date!='0'){

        //     data['where']['refund.entry_Date >= '] =  moment(date).format("YYYY-MM-DD");
        // }

        // if(date1!='0'){

        //     data['where']['refund.entry_Date <= '] =  moment(date1).format("YYYY-MM-DD");
        // }

        date = new Date();
        date.setDate(date.getDate() - 6);
        date.setFullYear(date.getFullYear() - 1);
        lastYear = date.getFullYear() + '-10-01'
        data['where']['refund.entry_Date >= '] = moment(lastYear).format("YYYY-MM-DD");
        // }

        // if(date1!='0'){

        data['where']['refund.entry_Date <= '] = moment().format("YYYY-MM-DD");
        // }


    }

    if (processor_type != 0) {

        data['where']['processors.type'] = processor_type;
    }


    let new_data = {};
    if (processor_type != 0) {
        if(stEmail == 1 || stEmail == "1"){
            new_data.where = {'processors.type': processor_type, 'processors.STEmail': 'True', 'processors.status': 1 };
        } else if(stEmail == 2 || stEmail == "2"){
            new_data.where = {'processors.type': processor_type, 'processors.STEmail': 'False',  'processors.status': 1 };
        } else {
            new_data.where = { 'processors.type': processor_type, 'processors.status': 1 };
        }
        // if (stEmail == 1 || stEmail == "1") {
        //     new_data.where = { 'processors.STEmail': 'True', 'processors.email != ': '', 'processors.status': 1 };
        // } else {
        //     new_data.where = { 'processors.type': processor_type, 'processors.status': 1,'processors.email != ': '' };
        // }
    } else {
        if(stEmail == 1 || stEmail == "1"){
            new_data.where = {'processors.STEmail': 'True','processors.status': 1 };
        } else if(stEmail == 2 || stEmail == "2"){
            new_data.where = {'processors.STEmail': 'False','processors.status': 1 };
        } else {
            new_data.where = {'processors.status': 1 };
        }
    }


    commonModel.getAll(constant.PROCESSORS, new_data).then(async function (rs) {
        //commonModel.getAll(constant.PROCESSORS,data).then(async function(rs) {

        console.log(query.last_query());

        if (rs.length) {

            for (const item in rs) {

                if (status == '1') {

                    var data = {

                        where: { "processor_id": rs[item].processor_id,"status !=":  2 },
                    };

                    if (date != 0) {

                        data['where']['assessments.date_Entered >= '] = moment(date).format("YYYY-MM-DD");
                    }

                    if (date1 != 0) {

                        data['where']['assessments.date_Entered <='] = moment(date1).format("YYYY-MM-DD");
                    }

                    rs[item].assesmentCount = await commonModel.getAll(constant.ASSESMENTS, data).then(async function (rs2) {
                        return rs2.length;
                    });

                } else {

                    var data = {

                        where: { "refund.processor_id": rs[item].processor_id },
                    };

                    if (date != 0) {

                        data['where']['refund.entry_Date >= '] = moment(date).format("YYYY-MM-DD");
                    }

                    if (date1 != 0) {

                        data['where']['refund.entry_Date <= '] = moment(date1).format("YYYY-MM-DD");
                    }

                    rs[item].assesmentCount = await commonModel.getAll(constant.REFUND, data).then(async function (rs2) {
                        return rs2.length;
                    });


                }

                rs[item].crd = moment(rs[item].crd).format("DD LL YYYY HH:mm a");

            };
        }

        res.json({ 'status': 'success', 'message': 'ok', 'count': rs.length, 'data': rs });
        return;
    });

}



exports.getDashboardCount = async function (req, res) {


    assesmentCount = await commonModel.getAll(constant.ASSESMENTS, { where: { 'status != ': 2 } }).then(async function (rs2) {
        return rs2.length;
    });

    producerCount = await commonModel.getAll(constant.POODUCERS, { where: { 'status != ': 2 } }).then(async function (rs2) {
        return rs2.length;
    });
    processorCount = await commonModel.getAll(constant.PROCESSORS, { where: { 'status != ': 2 } }).then(async function (rs2) {
        return rs2.length;
    });
    refundCount = await commonModel.getAll(constant.REFUND, { where: { 'status != ': 2 } }).then(async function (rs2) {
        return rs2.length;
    });
    reportsCount = await commonModel.getAll(constant.REPORT, { where: { 'status != ': 2 } }).then(async function (rs2) {
        return rs2.length;
    });


    res.json({ 'status': 'success', 'message': 'ok', 'assesmentsCount': assesmentCount, 'producerCount': producerCount, 'processorCount': processorCount, 'refundCount': refundCount, 'reportsCount': reportsCount });
    return;

}

exports.listRefund1 = async function (req, res) {

    let search = req.params.search;



    let data = {
        where: { "refund.status != ": 2 },
        join: "processors ON processors.processor_id=refund.processor_id",
        join1: "producers ON producers.producer_id=refund.producer_id",
        cols: "refund.id,refund.producer_id,refund.processor_id,refund.acssessType,refund.buTons,refund.amount,refund.status,processors.company,producers.firstName,producers.lastName,refund.entry_Date,processors.county_Num"
    };

    let data2 = {
        where: { "refund.status != ": 2 },
        join: "processors ON processors.processor_id=refund.processor_id",
        join1: "producers ON producers.producer_id=refund.producer_id",
        cols: "refund.id,refund.producer_id,refund.processor_id,refund.acssessType,refund.buTons,refund.amount,refund.status,processors.company,producers.firstName,producers.lastName,refund.entry_Date,processors.county_Num"
    };

    let processor_id = req.params.processor_id;
    let producer_id = req.params.producer_id;
    let date = req.params.date;
    let date1 = req.params.date1;
    let status = req.params.status;

    if (status == 2) {

        data['where']['refund.status'] = 0;
        data2['where']['refund.status'] = 0;

    } else {

        data['where']['refund.status'] = 1;
        data2['where']['refund.status'] = 1;
    }


    if (processor_id != 0) {
        data['where']['refund.processor_id'] = processor_id;
        data2['where']['refund.processor_id'] = processor_id;
        // data['whereIn1'] = {};
        // data['whereIn1']['key'] = 'refund.processor_id';
        // data['whereIn1']['value'] = JSON.parse(processor_id;);
    }


    if (producer_id != 0) {
        data['where']['refund.producer_id'] = producer_id;
        data2['where']['refund.producer_id'] = producer_id;
        // data['whereIn'] = {};
        // data['whereIn']['key'] = 'refund.producer_id';
        // data['whereIn']['value'] = JSON.parse(producer_id);
    }


    //  if(date!=0){

    //     data['where']['refund.entry_Date <='] =  moment(date).format("YYYY-MM-DD");
    //     data2['where']['refund.entry_Date <='] =  moment(date).format("YYYY-MM-DD");
    // }

    //    if(date1!=0){

    //     data['where']['refund.entry_Date >='] =  moment(date1).format("YYYY-MM-DD");
    //     data2['where']['refund.entry_Date >='] =  moment(date1).format("YYYY-MM-DD");
    // }
    if (date != 0) {

        data['where']['refund.entry_Date >='] = moment(date).format("YYYY-MM-DD");
        data2['where']['refund.entry_Date >='] = moment(date).format("YYYY-MM-DD");

    }

    if (date1 != 0) {
        date1 = moment(date1).format("YYYY-MM-DD");
        date1 = moment(date1).add(1, 'd').format("YYYY-MM-DD");
        data['where']['refund.entry_Date <='] = date1;
        data2['where']['refund.entry_Date <='] = date1;

    }
    // if (date != 0) {

    //     data['where']['refund.created_at >='] = moment(date).format("YYYY-MM-DD");
    // }

    // if (date1 != 0) {
    //     date1 = moment(date1).format("YYYY-MM-DD");
    //     date1 = moment(date1).add(1,'d').format("YYYY-MM-DD");
    //     data['where']['refund.created_at <='] = date1;
    // }


    if (search != 0) {

        if (isNaN(search)) {

            data2['like1'] = { 'processors.company': search };
            data2['or_like'] = { 'producers.firstName': search };
            rs2 = await commonModel.getAll(constant.REFUND, data2).then(async function (rs2) {

                rs = [];

                for (const item in rs2) {

                    rs[item] = rs2[item].id;

                };


                return rs;
            });

            data['whereIn'] = { 'key': 'refund.id', 'value': rs2 };


            //    data['like'] = {'producers.lastName':search};
        } else {
            data['where']['refund.producer_id'] = search;
            data['orWhere'] = { 'refund.processor_id': search, 'refund.id': search };
        }
    }


    commonModel.getAll(constant.REFUND, data).then(async function (rs) {
        let assiment_obj = {};
        let county_ids = [];
        for (let i in rs) {
            rs[i].acssessType = (rs[i].acssessType == '1') ? 'Grain' : 'Silage';
            rs[i].entry_Date = moment(rs[i].entry_Date).format("MM-DD-YYYY")
            rs[i].buTons = Number(rs[i].buTons);
            rs[i].amount = Number(rs[i].amount);
            
        
            if (!assiment_obj[rs[i].county_Num]) {
                assiment_obj[rs[i].county_Num] = [];
                county_ids.push(rs[i].county_Num);
            }
            assiment_obj[rs[i].county_Num].push(rs[i]);
        }

        let county_data = {
            'whereIn': { 'key': 'County_Num', 'value': county_ids }
        };
        let countyResponse = await commonModel.getAll(constant.COUNTIES, county_data)

        let county_obj = {};
        let region_ids = [];
        for (let j in countyResponse) {
            if (!county_obj[countyResponse[j].Region_Id]) {
                county_obj[countyResponse[j].Region_Id] = [];
                region_ids.push(countyResponse[j].Region_Id);
            }
            county_obj[countyResponse[j].Region_Id].push(countyResponse[j]);

        }
        let region_data = {
            'whereIn': { 'key': 'id', 'value': region_ids }
        }
        let regionResponse = await commonModel.getAll(constant.REGION, region_data)
        if (rs.length) {
            html = '';
            bTotal = 0;
            aTotal = 0;

            let baseUrl = req.protocol + '://' + req.headers['host'];

            rs.forEach(function (rs1) {
              //  rs1.acssessType = (rs1.acssessType == '1') ? 'Grain' : 'Silage';
               // html += ' <tr style="border: 1px solid black;"><td style="text-align: center; font-size: 8px;">' + rs1.id + '</td>         <td  style="text-align: center; font-size: 8px;">' + rs1.company + '</td>  <td  style="text-align: center; font-size: 8px;">' + rs1.firstName + ' ' + rs1.lastName + '</td> <td  style="text-align: center; font-size: 8px;">' + rs1.acssessType + '</td>          <td  style="text-align: center; font-size: 8px;">' + moment(rs1.entry_Date).format("MM-DD-YYYY") + '</td>         <td  style="text-align: center; font-size: 8px;">' + rs1.buTons + '</td> <td  style="text-align: center; font-size: 8px;">' + rs1.amount + '</td>       </tr>';
                bTotal = Number(rs1.buTons)+bTotal;
                aTotal = Number(rs1.amount)+aTotal;
               

            });

            // html1 = ' <tr><td style="text-align: center; font-size: 8px;border: 1px solid black;" colspan="5"  align="right"><b>Total Butons</b></td><td style="text-align: left; font-size: 8px;border: 1px solid black;" colspan="2"  align="left">' + (bTotal).toFixed(2) + '</td></tr>';
            // html1 += ' <tr><td style="text-align: center; font-size: 8px;border: 1px solid black;" colspan="5"  align="right"><b>Total Amount</b></td><td style="text-align: left; font-size: 8px;border: 1px solid black;" colspan="2" align="left">' + (aTotal).toFixed(2) + '</td></tr>';

            //tabledata = ' <div style="text-align: center;">Refund List</div><br><table style="width: 100%;  margin: 0 auto;border: 1px solid black;">  <tr style="line-height: 40px;"> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">REFUND ID</p></th>          <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">PRODUCERS</p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">PROCESSORS</p></th>   <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">ACCESSTYPE</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">DATE ENTERED</p></th>  <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">BUTONS</p></th> <th><p style="font-size: 10px; line-height: 20px; font-weight: 100;">AMOUNT</p></th></tr>' + html + html1 + '</table>';

            var template = process.cwd() + '/app/templates/refundreport.ejs';
            var templateData = {
                 data: rs,
                bTotal: bTotal,
                aTotal: aTotal,
                county_obj :county_obj,
                regionResponse:regionResponse,
                assiment_obj:assiment_obj
            };
            if(template && templateData){
            ejs.renderFile(template, templateData, 'utf8', function (err, file) {
            name = Date.now();
            url = baseUrl + "/" + name + ".pdf";
            var options = { format: 'Letter' };

            pdf.create(file, options).toFile('./public/' + name + '.pdf', async function (err, rs) {

                res.json({ 'status': 'success', 'message': 'ok', 'url': url });
                return

            });
        });
        }
        } else {

            res.json({ 'status': 'fail', 'message': 'No record found', url: [] });
            return;

        }


    });
}
