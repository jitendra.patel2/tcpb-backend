service = require('../app/controllers/serviceCtrl');//you can include all your controllers
producers = require('../app/controllers/producersCtrl');//you can include all your controllers
processors = require('../app/controllers/processorsCtrl');//you can include all your controllers
user = require('../app/controllers/userCtrl');//you can include all your controllers
refund = require('../app/controllers/refundCtrl');//you can include all your controllers
assessments = require('../app/controllers/assessmentsCtrl');//you can include all your controllers
notes = require('../app/controllers/notesCtrl');//you can include all your controllers
stripe = require('../app/controllers/stripeCtrl');//you can include all your controllers

module.exports = function (app) {
	 app.post('/api/admin/login',service.login);
	 app.post('/api/admin/forgetPassword',service.forgetPassword);
	 app.post('/api/admin/createUser',user.createUser);
	 app.post('/api/admin/updateUser/:id',user.updateUser);
	 app.post('/api/admin/changePassword/:id',user.changePassword);
	 app.get('/api/admin/updateUserStatus/:id/:status',user.updateUserStatus);
	 app.get('/api/admin/getUserById/:id',user.getUserById);
	 app.get('/api/admin/listUser/:search',user.listUser);
	 app.get('/api/admin/roleList',user.roleList);
	 app.get('/api/admin/countiesList',user.countiesList);
	 app.get('/api/admin/getState',user.getState);
	 app.post('/api/admin/createProducers',producers.createProducers);
	 app.post('/api/admin/updateProducers/:id',producers.updateProducers);
	 app.get('/api/admin/updateProducersStatus/:id/:status',producers.updateProducersStatus);
	 app.get('/api/admin/getProducersById/:id',producers.getProducersById);
	 app.get('/api/admin/listProducers/:search',producers.listProducers);
	 app.get('/api/admin/getProducers',producers.getProducers);
	 app.post('/api/admin/createProcessors',processors.createProcessors);
	 app.post('/api/admin/updateProcessors/:id',processors.updateProcessors);
	 app.get('/api/admin/updateProcessorsStatus/:id/:status',processors.updateProcessorsStatus);
	 app.get('/api/admin/getProcessorsById/:id',processors.getProcessorsById);
	 app.get('/api/admin/listUProcessors/:search',processors.listUProcessors);
	 app.get('/api/admin/getProcessors',processors.getProcessors);
	 app.post('/api/admin/sendEmail',processors.sendEmail);
	 app.post('/api/admin/createRefund',refund.createRefund);
	 app.post('/api/admin/updateRefund/:id',refund.updateRefund);
	 app.get('/api/admin/updateRefundStatus/:id/:status',refund.updateRefundStatus);
	 app.get('/api/admin/getRefundById/:id',refund.getRefundById);
	 app.get('/api/admin/listRefund/:processor_id/:producer_id/:date/:search/:status/:date1',refund.listRefund);
	 app.post('/api/admin/createAssessments',assessments.createAssessments);
	 app.post('/api/admin/updateAssessments/:id',assessments.updateAssessments);
	 app.get('/api/admin/updateAssessmentsStatus/:id/:status',assessments.updateAssessmentsStatus);
	 app.get('/api/admin/getAssessmentsById/:id',assessments.getAssessmentsById);
	 app.get('/api/admin/listAssessments/:processor_id/:accessType/:date/:search/:status/:date1/:deposit_Num',assessments.listAssessments);
	 app.get('/api/admin/listNotes/:id',notes.listNotes);
	 app.get('/api/admin/geNotesById/:id',notes.geNotesById);
	 app.post('/api/admin/updateNotes/:id',notes.updateNotes);
	 app.get('/api/admin/updateNotesStatus/:id/:status',notes.updateNotesStatus);
	 app.get('/api/admin/genrateCsv',assessments.genrateCsv);
	 app.post('/api/admin/sendAssmentReport',assessments.sendAssessmentsReport);	
	 app.get('/api/admin/getReport/:date/:status/:date1/:reportType/:stEmail/:processor_type',refund.getReport);
	 app.get('/api/admin/getReportList',refund.getReportList);
	 app.post('/api/admin/saveQuery',refund.saveQuery);		
	 app.get('/api/admin/listQuery',refund.listQuery);		
	 app.get('/api/admin/getQueryById/:id',refund.getQueryById);
	 app.get('/api/admin/getReportListById/:id/:type',refund.getReportListById);
	 app.get('/api/admin/getProcessorCount/:date/:status/:date1/:stEmail/:processor_type',refund.getProcessorCount);
	 app.get('/api/admin/getDashboardCount',refund.getDashboardCount);
	 app.get('/api/admin/listAssessments1/:processor_id/:accessType/:date/:search/:status/:date1/:deposit_Num',assessments.listAssessments1);
	 app.get('/api/admin/listRefund1/:processor_id/:producer_id/:date/:search/:status/:date1',refund.listRefund1);
	 app.post('/api/admin/stripePyament1',stripe.stripePaymentCard);	
	 app.post('/api/admin/stripePyament',stripe.stripePayment);	
	 app.post('/api/admin/stripetest',stripe.stripetest);	
}
