const Common = require('../models/commonModel.js');//you can include all your lib
let commonModel = new Common();
var md5 = require('md5');
var country = require('country-state-picker');

exports.getState = function (req, res) {

    res.json({ 'status': 'success', 'message': 'ok', data: country.getStates('us') });
    return


}

exports.createUser = async function (req, res) {

    fields = req.body;
    let email = fields.email;
    let first_name = fields.firstName;
    let last_name = fields.lastName;
    let phone_number = fields.phone;
    let address1 = fields.address;
    let address2 = fields.address2;
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let user_role = fields.role;
    let password = fields.password;
    let country = fields.country;

    if (first_name == '') {

        res.json({ status: "fail", message: 'first name is required' });
        return;

    } else if (last_name == '') {

        res.json({ status: "fail", message: 'last name is required' });
        return;

    } else if (email == '') {

        res.json({ status: "fail", message: 'email is required' });
        return;

    } else if (phone_number == '') {

        res.json({ status: "fail", message: 'phone number is required' });
        return;

    } else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({status: "fail",message: 'address2 is required'});
    //     return;

    // } 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    } else if (user_role == '') {

        res.json({ status: "fail", message: 'user role is required' });
        return;

    } else if (password == '') {

        res.json({ status: "fail", message: 'Password is required' });
        return;

    }



    let data = {
        where: { 'email': email, 'status !=': 2 }
    }



    commonModel.getAll(constant.USERS, data).then(async function (user) {

        user = user[0];
        if (user) {
            res.json({ status: "fail", message: 'Email id already registred' });
            return;

        } else {


            var option = {

                'email': email,
                'first_name': first_name,
                'last_name': last_name,
                'phone_number': phone_number,
                'country': country,
                'address1': address1,
                'address2': address2,
                'city': city,
                'state': state,
                'zip': zip,
                'user_role_id': user_role,
                'password': md5(password),
            }

            commonModel.save(constant.USERS, option).then(function (user) {

                res.json({ 'status': 'success', 'message': 'User Added successfully', 'data': user });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                return;
            });
        }

    });

}


exports.updateUser = async function (req, res) {

    fields = req.body;
    let email = fields.email;
    let first_name = fields.firstName;
    let last_name = fields.lastName;
    let phone_number = fields.phone;
    let address1 = fields.address;
    let address2 = fields.address2;
    let city = fields.city;
    let state = fields.state;
    let zip = fields.postalCode;
    let user_role = fields.role;
    let password = fields.password;
    let country = fields.country;
    let id = req.params.id;

    if (first_name == '') {

        res.json({ status: "fail", message: 'first name is required' });
        return;

    } else if (last_name == '') {

        res.json({ status: "fail", message: 'last name is required' });
        return;

    } else if (email == '') {

        res.json({ status: "fail", message: 'email is required' });
        return;

    } else if (phone_number == '') {

        res.json({ status: "fail", message: 'phone number is required' });
        return;

    } else if (address1 == '') {

        res.json({ status: "fail", message: 'address1 is required' });
        return;

    }
    // else if (address2 == '') {

    //     res.json({status: "fail",message: 'address2 is required'});
    //     return;

    // } 
    else if (city == '') {

        res.json({ status: "fail", message: 'city is required' });
        return;

    } else if (state == '') {

        res.json({ status: "fail", message: 'state is required' });
        return;

    } else if (zip == '') {

        res.json({ status: "fail", message: 'zip is required' });
        return;

    } else if (user_role == '') {

        res.json({ status: "fail", message: 'user role is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }



    let data = {
        where: {
            'email': email,
            'id != ': id,
            'status !=': 2
        }
    }

    commonModel.getAll(constant.USERS, data).then(async function (user) {

        user = user[0];

        if (user) {

            res.json({ status: "fail", message: 'Email id already registred' });
            return;

        } else {


            var option = {

                'email': email,
                'first_name': first_name,
                'last_name': last_name,
                'phone_number': phone_number,
                'address1': address1,
                'address2': address2,
                'city': city,
                'state': state,
                'zip': zip,
                'country': country,
                'user_role_id': user_role,
            }

            let where = { 'id': id };

            commonModel.updateData(constant.USERS, option, where).then(function (user) {

                res.json({ 'status': 'success', 'message': 'User updated successfully' });
                return;

            }).catch(function (error) {

                res.json({ 'status': 'fail', 'message': 'Something went wrong' });
                return;
            });
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.updateUserStatus = async function (req, res) {

    let status = req.params.status;
    let id = req.params.id;

    if (status == '') {

        res.json({ status: "fail", message: 'status is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    var option = {

        'status': status,
    }

    let where = { 'id': id };

    commonModel.updateData(constant.USERS, option, where).then(function (user) {

        res.json({ 'status': 'success', 'message': 'User updated successfully' });
        return;

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong' });
        return;
    });

}

exports.getUserById = async function (req, res) {

    let id = req.params.id;

    if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }


    commonModel.getAll(constant.USERS, { where: { "id": id } }).then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs[0] });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}

exports.listUser = function (req, res) {

    let data = {
        where: { "users.user_role_id != ": 1, "users.status != ": 2 },
        join: "role ON role.id=users.user_role_id",
        cols: "users.id,users.first_name,users.last_name,users.email,users.phone_number,users.status,role.role,users.user_role_id",
        orderBy: "id DESC"
    };

    let search = req.params.search;

    // if(search!=0){
    //      if(isNaN(search)){
    //         search = search.split(" ");
    //         data['like'] = {'users.first_name':search[0]};
    //         if(search.length > 1){
    //             data['or_like'] = {'users.last_name':search[1]};
    //         }

    //         // data['like'] = {'users.first_name':search};
    //         // data['or_like'] = {'users.last_name':search};

    //       //  data['or_like'] = {'users.last_name':search};


    //     }else{
    //          data['where']['id'] = search;
    //     }



    // }
    if (search != 0) {
        if (isNaN(search)) {
            let search_name = search.split(" ");
            if(search.length > 1){  
                if(search_name[0]){
                data['like'] = {'users.first_name':search_name[0]};
                }
                if(search_name[1]){
                // data['or_like'] = {'users.last_name':search_name[1]};
                 data['like1'] = {'users.last_name':search_name[1]};
                }
                // data['or_like'] = {'users.last_name':search};
                }else{
                data['like'] = {'users.first_name':search};
                data['or_like'] = {'users.last_name':search};
                }
        } else {
            data['where']['id'] = search;
        }
    }

    commonModel.getAll(constant.USERS, data).then(function (rs) {
        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'success', 'message': 'No record found', data: [] });
            return;

        }

    });
}

exports.roleList = function (req, res) {


    commonModel.getAll(constant.ROLE, '').then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}

exports.countiesList = function (req, res) {


    commonModel.getAll(constant.COUNTIES, '').then(function (rs) {

        if (rs.length) {

            res.json({ 'status': 'success', 'message': 'ok', data: rs });
            return

        } else {

            res.json({ 'status': 'fail', 'message': 'No record found' });
            return;

        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });
}




exports.changePassword = async function (req, res) {

    fields = req.body;
    let oldPassword = fields.oldPassword;
    let newPassword = fields.newPassword;
    let email = fields.email;

    let id = req.params.id;
    if (oldPassword == '') {

        res.json({ status: "fail", message: 'oldPassword is required' });
        return;

    } else if (newPassword == '') {

        res.json({ status: "fail", message: 'newPassword is required' });
        return;

    } else if (id == '') {

        res.json({ status: "fail", message: 'id is required' });
        return;

    }

    let data = {
        'id': id,
        'email': email
    }

    commonModel.getAll(constant.USERS, data).then(async function (user) {
        user = user[0];
        // if (err) {
        //     res.status(500);
        //     res.json({ status: "fail",  message: err});
        //     return;
        // } else if (!user) {
        //     if(user.password!=md5(oldPassword)){
        //         res.json({'status':'fail',"message":"Old password not match"});
        //         return;
        //     }else{

        //          var option = {
        //             'password' : md5(newPassword),
        //         }

        //         let where = {'id':id};
        //         console.log('11111111111111111111',where,id);
        //         commonModel.updateData(constant.USERS,option,where).then(function(user) {

        //             res.json({'status':'success','message':'Password updated successfully'});
        //             return;  

        //         }).catch(function(error){
        //             res.json({'status':'fail','message':'Something went wrong','data':error});
        //             return;
        //         });
        //     }
        // }
        console.log('QQQQQQQQQQQQQQQQQQQQQQQQQQQ', email);
        if (user) {
            if (user.password != md5(oldPassword)) {
                res.json({ 'status': 'fail', "message": "Old password not match" });
                return;
            } else if (user.email != email) {
                res.json({ 'status': 'fail', "message": "Email not match" });
                return;
            } else {

                var option = {
                    'password': md5(newPassword),
                }

                let where = { 'id': id };
                commonModel.updateData(constant.USERS, option, where).then(function (user) {

                    res.json({ 'status': 'success', 'message': 'Password updated successfully' });
                    return;

                }).catch(function (error) {
                    res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
                    return;
                });
            }
        }

    }).catch(function (error) {

        res.json({ 'status': 'fail', 'message': 'Something went wrong', 'data': error });
        return;
    });

}
